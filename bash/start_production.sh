#!/usr/bin/env bash

pm2 kill
rm -rf ~/FishPlaces/build
unzip -o ~/FishPlaces/artifact.zip
rm -rf ~/FishPlaces/artifact.zip
pm2 start ~/FishPlaces/ecosystem.config.js --only fish-places-production
#!/usr/bin/env bash
