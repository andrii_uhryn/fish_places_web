const fs = require('fs');
const path = require('path');
const paths = require('./paths');

delete require.cache[require.resolve('./paths')];

if (!process.env.NODE_ENV) {
    throw new Error(
        'The process.env.NODE_ENV environment variable is required but was not specified.',
    );
}

if (fs.existsSync(`${paths.dotenv}.${process.env.NODE_ENV_PRIMARY}`)) {
    require('dotenv').config({
        path: `${paths.dotenv}.${process.env.NODE_ENV_PRIMARY}`,
    });
}

const appDirectory = fs.realpathSync(process.cwd());
process.env.NODE_PATH = (process.env.NODE_PATH || '')
    .split(path.delimiter)
    .filter((folder) => folder && !path.isAbsolute(folder))
    .map((folder) => path.resolve(appDirectory, folder))
    .join(path.delimiter);

module.exports = () => {
    // define env vars you want to use in your client app here.
    // CAREFUL: don't use any secrets like api keys or database passwords as they are exposed publicly!

    const raw = {
        PORT: process.env.PORT || 8500,
        HOST: process.env.HOST || 'http://localhost',

        NODE_ENV: process.env.NODE_ENV || 'development',
        NODE_ENV_PRIMARY: process.env.NODE_ENV_PRIMARY,

        FB_ID: process.env.FB_ID,
        GTM_ID: process.env.GTM_ID,

        BACKEND_URL: process.env.BACKEND_URL,

        CLOUDINARY_NAME: process.env.CLOUDINARY_NAME,
        CLOUDINARY_FOLDER: process.env.CLOUDINARY_FOLDER,
        CLOUDINARY_API_KEY: process.env.CLOUDINARY_API_KEY,
        CLOUDINARY_UPLOAD_PRESET: process.env.CLOUDINARY_UPLOAD_PRESET,

        GOOGLE_BOT_VERIFICATION: process.env.GOOGLE_BOT_VERIFICATION,

        PRISMIC_API: process.env.PRISMIC_API,
        PRISMIC_TOKEN: process.env.PRISMIC_TOKEN,
    };

    // Stringify all values so we can feed into Webpack DefinePlugin
    const stringified = {
        'process.env': Object.keys(raw).reduce((env, key) => {
            env[key] = JSON.stringify(raw[key]);
            return env;
        }, {}),
    };

    return { raw, stringified };
};
