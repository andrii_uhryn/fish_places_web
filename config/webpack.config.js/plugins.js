const webpack = require('webpack');
const CopyPlugin = require('copy-webpack-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');

const paths = require('../paths');
const { clientOnly } = require('../../scripts/utils');

const env = require('../env')();

const shared = [
    new MiniCssExtractPlugin({
        filename:
            process.env.NODE_ENV === 'development' ? '[name].css' : '[name].[contenthash].css',
        chunkFilename:
            process.env.NODE_ENV === 'development' ? '[id].css' : '[id].[contenthash].css',
    }),
    new CopyPlugin([
        {
            to: `${paths.clientBuild}${paths.publicPath}assets/`,
            from: 'icons/*.png',
        },
    ]),
];

const client = [
    clientOnly && new HtmlWebpackPlugin({
        inject: true,
        template: paths.appHtml,
    }),
    new CaseSensitivePathsPlugin(),
    new webpack.DefinePlugin(env.stringified),
    new webpack.DefinePlugin({
        __SERVER__: 'false',
        __BROWSER__: 'true',
    }),
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
    new ManifestPlugin({
        fileName: 'manifest.json',
        seed: {
            'short_name': 'GoodCrew',
            'name': 'GoodCrew',
            'start_url': '.',
            'display': 'standalone',
            'theme_color': '#000000',
            'background_color': '#ffffff',
            'icons': [],
        },
    }),
    new CompressionPlugin({
        algorithm: 'gzip',
        test: /\.js/,
        threshold: 10240,
        minRatio: 0.8
    })
].filter(Boolean);

const server = [
    new webpack.DefinePlugin({
        __SERVER__: 'true',
        __BROWSER__: 'false',
    }),
];

module.exports = {
    shared,
    client,
    server,
};
