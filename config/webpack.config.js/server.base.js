const path = require('path');

const paths = require('../paths');
const plugins = require('./plugins');
const resolvers = require('./resolvers');
const { server: serverLoaders } = require('./loaders');

module.exports = {
    name: 'server',
    target: 'node',
    entry: {
        server: [
            require.resolve('core-js/stable'),
            require.resolve('regenerator-runtime/runtime'),
            path.resolve(paths.srcServer, 'index.js'),
        ],
    },
    output: {
        path: paths.serverBuild,
        filename: 'server.js',
        publicPath: paths.publicPath,
        libraryTarget: 'commonjs2',
    },
    resolve: { ...resolvers },
    module: {
        rules: serverLoaders,
    },
    plugins: [...plugins.shared, ...plugins.server],
    stats: {
        colors: true,
    },
};
