const webpack = require('webpack');
const nodeExternals = require('webpack-node-externals');
const WriteFileWebpackPlugin = require('write-file-webpack-plugin');

const baseConfig = require('./server.base');

const config = {
    ...baseConfig,
    plugins: [
        new WriteFileWebpackPlugin(),
        ...baseConfig.plugins,
        new webpack.HotModuleReplacementPlugin(),
    ],
    mode: 'development',
    performance: {
        hints: false,
    },
    externals: [
        nodeExternals({
            // we still want imported css from external files to be bundled otherwise 3rd party packages
            // which require us to include their own css would not work properly
            whitelist: /\.css$/,
        }),
    ],
};

module.exports = config;
