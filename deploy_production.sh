#!/usr/bin/env bash

rm -rf artifact.zip

printf "\nStart project build\n"
npm run build
printf "\nEnd project build\n"

printf "\nStart zip file creating\n"
zip -r artifact.zip build bash ecosystem.config.js
printf "\nEnd zip file creating\n"

printf "\nStart artifact file transfer to Server\n"
scp ./artifact.zip root@thefishplaces.com:/root/FishPlaces
printf "\nEnd artifact file transfer to Server\n"

rm -rf artifact.zip
rm -rf build
