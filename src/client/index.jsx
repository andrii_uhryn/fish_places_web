import React from 'react';
import { hydrate } from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter as Router } from 'connected-react-router';

import Root from '../shared/Root';
import IntlProvider from '../shared/i18n/IntlProvider';

import createHistory from '../shared/store/history';
import { configureStore } from '../shared/store';

const history = createHistory({});

const store =
    window.store ||
    configureStore({
        initialState: window.__PRELOADED_STATE__,
        history,
    });

hydrate(
    <Provider store={store}>
        <Router history={history}>
            <IntlProvider>
                <Root />
            </IntlProvider>
        </Router>
    </Provider>,
    document.getElementById('app'),
);

if (process.env.NODE_ENV === 'development') {
    if (module.hot) {
        module.hot.accept();
    }

    if (!window.store) {
        window.store = store;
    }
}
