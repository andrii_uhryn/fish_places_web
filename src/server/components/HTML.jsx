import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';

import {
    fbScript,
    googleTagManager,
    googleTagNoScript,
} from '../../shared/constants/scripts';

const HTML = ({ children, css = [], scripts = [], state = '{}' }) => {
    const head = Helmet.renderStatic();

    return (<html {...head.htmlAttributes.toComponent()}>
    <head>
        {process.env.NODE_ENV_PRIMARY === 'production' ? googleTagManager : ''}
        {head.base.toComponent()}
        {head.title.toComponent()}
        {head.meta.toComponent()}
        {head.link.toComponent()}
        {head.script.toComponent()}
        {head.style.toComponent()}
        {css.filter(Boolean).map((href) => (
            <link key={href} rel="stylesheet" href={href} />
        ))}
        <link rel="manifest" href="/static/manifest.json" />
    </head>
    <body>
    {process.env.NODE_ENV_PRIMARY === 'production' ? googleTagNoScript : ''}
    {process.env.NODE_ENV_PRIMARY === 'production' ? fbScript : ''}
    <script
        dangerouslySetInnerHTML={{
            __html: `window.__PRELOADED_STATE__ = ${state}`,
        }}
    />
    <div id="app" dangerouslySetInnerHTML={{ __html: children }} />
    {scripts.filter(Boolean).map((src) => {
        if (process.env.NODE_ENV === 'production') {
            return <script key={src} type="text/javascript" src={`${src}.gz`} />;
        }

        return <script key={src} type="text/javascript" src={src} />;
    })}
    </body>
    </html>);
};

HTML.propTypes = {
    children: PropTypes.any,
    css: PropTypes.arrayOf(PropTypes.string),
    scripts: PropTypes.arrayOf(PropTypes.string),
    scss: PropTypes.arrayOf(PropTypes.string),
    state: PropTypes.string,
};

export default HTML;
