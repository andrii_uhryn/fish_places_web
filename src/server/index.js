import path from 'path';
import cors from 'cors';
import chalk from 'chalk';
import express from 'express';
import bodyParser from 'body-parser';
import compression from 'compression';
import manifestHelpers from 'express-manifest-helpers';
import cookiesMiddleware from 'universal-cookie-express';

import paths from '../../config/paths';
import { errorHandler, serverRenderer } from './middleware';

require('dotenv').config();

const app = express();

app.get('*.js.gz', function (req, res, next) {
    res.set('Content-Encoding', 'gzip');
    res.set('Content-Type', 'application/javascript');
    next();
});
app.use(paths.publicPath, express.static(path.join(paths.clientBuild, paths.publicPath)));

app.use(cors());
app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookiesMiddleware());
app.set('trust proxy', true);
app.set('trust proxy', 'loopback');

const manifestPath = path.join(paths.clientBuild, paths.publicPath);

app.use(
    manifestHelpers({
        manifestPath: `${manifestPath}/manifest.json`,
    }),
);

app.use(serverRenderer);

app.use(errorHandler);

app.listen(process.env.PORT || 8500, () => {
    console.log(    // eslint-disable-line
        `[${new Date().toISOString()}]`,
        chalk.blue(`App is running: 🌎 http://localhost:${process.env.PORT || 8500}`),
    );
});

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

export default app;
