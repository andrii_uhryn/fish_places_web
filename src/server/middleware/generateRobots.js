import _uniq from 'lodash/uniq';

import routes from '../../shared/routes';
const baseUrl = `https://${process.env.NODE_ENV_PRIMARY === 'development' ? 'staging.' : ''}thefishplaces.com`;

const renderItem = item => _uniq(item.words.map(word => `Disallow: ${item.url.replace(':url', word)}`)).join('\n');

export default (req, res) => {
	const urlsToDisallow = [];
	const getUrls = item => {
		if (item.disallow && item.disallowUrl) {
			urlsToDisallow.push({ url: item.disallowUrl, words: item.disallowWords });
		}

		if (item.routes) {
			item.routes.forEach(getUrls);
		}
	};

	if (routes && routes.length) {
		routes.forEach(getUrls);
	}

	res.type('text/plain; charset=UTF-8');
	res.send(`
# robots.txt for ${baseUrl}

User-agent: *
${urlsToDisallow.map(renderItem).join('\n')}

Sitemap: ${baseUrl}/sitemap.xml
	`);
};
