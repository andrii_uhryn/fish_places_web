import moment from 'moment/moment';

import routes from '../../shared/routes';
import { getLink } from '../../shared/utils/urlHelper';

import { LANGUAGES } from '../../shared/constants/general';

const baseUrl = `https://${process.env.NODE_ENV_PRIMARY === 'development' ? 'staging.' : ''}thefishplaces.com`;

const renderItem = item => {
    let idUrls = [item];
    let urlArr = [];

    idUrls.forEach(el => {
        if (el.url.includes('(')) { // handle multi language urls.
            urlArr = [...urlArr, ...handleMultiLanguage(el.url).map(translatedUrl => ({
                ...el,
                url: translatedUrl
            }))];
        } else {
            urlArr.push(el);
        }
    });

    return urlArr.map(el => `
        <url>
            <loc>${baseUrl}${el.url}</loc>
            ${el.updated_at ? `<lastmod>${moment(el.updated_at).format('YYYY-MM-DD')}</lastmod>` : ''}
            ${getTranslated(el.url).map(item => `<xhtml:link rel="alternate" hreflang="${item.language}" href="${baseUrl}${item.url}" />`).join(' ')}
        </url>
    `).join(' ');
};
const generateList = cb => {
    const sitemap = [];
    const getUrls = item => {
        if (!item.disallow && item.path) {
            sitemap.push({ url: item.path });
        }

        if (!item.disallowChildren && item.routes) {
            item.routes.forEach(getUrls);
        }
    };

    if (routes && routes.length) {
        routes.forEach(getUrls);
    }

    cb(sitemap);
};
const handleMultiLanguage = item => {
    const newTxt = item.split('(');
    const urlArr = [];

    let stringMulti = [];

    for (let i = 1; i < newTxt.length; i++) {
        stringMulti.push(newTxt[i].split(')')[0]);
    }

    if (stringMulti.length) {
        stringMulti = stringMulti.map(strItem => strItem.split('|'));

        for (let i = 0; i < stringMulti[0].length; i++) {
            let urlToPush = item;

            stringMulti.forEach(strToReplace => {
                urlToPush = urlToPush.replace(/\(([^)]+)\)/, strToReplace[i]);
            });

            urlArr.push(urlToPush);
        }
    }

    return urlArr;
};

export const getTranslated = (url) => {
    const language = url.split('/')[1];

    return LANGUAGES.map(item => {
        if (item.id === language) {
            return {
                url,
                language,
            };
        }

        return {
            url: getLink({ language: item.id, location: { pathname: url } }).pathname,
            language: item.id,
        };
    });
};

export default (req, res) => generateList(sitemap => {
    res.type('text/xml');
    res.send(`<?xml version="1.0" encoding="UTF-8"?>
		<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xhtml="http://www.w3.org/1999/xhtml" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd http://www.w3.org/1999/xhtml http://www.w3.org/2002/08/xhtml/xhtml1-strict.xsd">
			${sitemap.map(renderItem).join(' ')}
		</urlset>
	`);
});
