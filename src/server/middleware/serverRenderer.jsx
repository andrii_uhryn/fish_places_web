import 'isomorphic-fetch';
import React from 'react';
import _findIndex from 'lodash/findIndex';
import queryString from 'query-string';
import { Provider } from 'react-redux';
import { renderToString } from 'react-dom/server';
import { StaticRouter as Router, matchPath } from 'react-router-dom';

import Root from '../../shared/Root';
import Html from '../components/HTML';
import IntlProvider from '../../shared/i18n/IntlProvider';

import routes from '../../shared/routes';

import { getLink } from '../../shared/utils/urlHelper';
import createHistory from '../../shared/store/history';
import generateRobots from './generateRobots';
import generateSiteMap from './generateSiteMap';
import { configureStore } from '../../shared/store';
import { changeLanguage, setBaseUrl, setMode } from '../../shared/actions/root';

import { DEFAULT_LANGUAGE, LANGUAGES } from '../../shared/constants/general';
import { MOBILE_REGEXP, TABLET_REGEXP } from '../../shared/constants/regExp';

export const serverRenderer = (req, res, next) => {
    if (req.url === '/sitemap.xml') {
        return generateSiteMap(req, res, next);
    } else if (req.url === '/robots.txt') {
        return generateRobots(req, res, next);
    }

    const cookies = req.universalCookies.getAll();
    const language = (req.url.split('/')[1] || '').split('?')[0];
    const history = createHistory({ initialEntries: [req.url] });
    const baseUrl = `https://${process.env.NODE_ENV_PRIMARY === 'development' ? 'staging.' : ''}thefishplaces.com`;
    const SERVER_HOST = process.env.SERVER_HOST || 'http://localhost';
    const isDevelopment = process.env.NODE_ENV === 'development';
    const { query: { ...filters } } = queryString.parseUrl(req.url);
    const promises = [];

    let mode = 'desktop';

    if (TABLET_REGEXP.test(req.headers['user-agent'])) {
        mode = 'tablet';
    } else if (MOBILE_REGEXP.test(req.headers['user-agent'])) {
        mode = 'mobile';
    }

    if (!language || !~_findIndex(LANGUAGES, { id: language })) {
        const navigatorLanguage = req.headers['accept-language'] && req.headers['accept-language'].slice(0, 2);
        let langToRedirect = DEFAULT_LANGUAGE;

        if (~_findIndex(LANGUAGES, { id: navigatorLanguage })) {
            langToRedirect = navigatorLanguage;
        }

        return res.redirect(`/${langToRedirect}`);
    }

    const store = configureStore({ history });

    store.dispatch(changeLanguage(language));
    store.dispatch(setBaseUrl(baseUrl));

    if (mode !== 'desktop') {
        store.dispatch(setMode(mode));
    }

    if (cookies.token) {
        //promises.push(store.dispatch(fetchUser(req.universalCookies)));
    }

    let currentRoute;

    const findComponent = (routes, path) => routes.forEach(route => {
        const obj = matchPath(path, route);

        if (obj && obj.isExact) {
            currentRoute = route;
            currentRoute.params = obj.params;
        } else if (route.routes) {
            findComponent(route.routes, path);
        }
    });

    findComponent(routes, req.url.split('?')[0]);

    if (currentRoute && typeof currentRoute.component?.initialFetch === 'function') {
        const fetchProps = {
            mode,
            filters,
            language,
            cookies: req.universalCookies,
        };

        if (currentRoute.params?.id) {
            fetchProps.id = currentRoute.params.id;
        }

        promises.push(currentRoute.component.initialFetch(store, fetchProps));
    }

    return Promise
        .allSettled(promises)
        .then((resArr) => {
            const rejected = resArr.filter(item => item.status === 'rejected');

            if (rejected.length) {
                const linkProps = { language, location: { pathname: req.url } };
                let linkToRedirect = '';

                rejected.forEach(error => {
                    if (~[401, 498].indexOf(error.status)) {
                        linkToRedirect = getLink({ ...linkProps, component: 'signIn' });
                    }
                });

                if (linkToRedirect) {
                    res.redirect(linkToRedirect.pathname);
                }
            } else {
                const state = JSON.stringify(store.getState());

                let css = [res.locals.assetPath('bundle.css'), res.locals.assetPath('vendor.css')];
                let scripts = [res.locals.assetPath('bundle.js'), res.locals.assetPath('vendor.js')];

                if (!isDevelopment) {
                    css = css.map(item => {
                        item = `${SERVER_HOST}${item}`;

                        return item;
                    });

                    scripts = scripts.map(item => {
                        item = `${SERVER_HOST}${item}`;

                        return item;
                    });
                }

                if (process.env.NODE_ENV_PRIMARY !== 'production') {
                    res.header('X-Robots-Tag', 'noindex');
                }

                if (process.env.NODE_ENV === 'production') {
                    res.header('Content-Security-Policy', 'upgrade-insecure-requests; default-src \'unsafe-eval\' https: https://noembed.com; connect-src https: https://noembed.com; font-src https: data: https://noembed.com; frame-src https: https://noembed.com; frame-ancestors https:; img-src https: data: https://noembed.com; media-src https:; object-src https:; script-src \'unsafe-inline\' \'unsafe-eval\' https: https://noembed.com; style-src \'unsafe-inline\' https:;');
                }

                return res.send(
                    '<!doctype html>' +
                    renderToString(
                        <Html
                            css={css}
                            state={state}
                            scripts={scripts}
                        >
                            {
                                renderToString(<Provider store={store}>
                                    <Router location={req.url} context={{}}>
                                        <IntlProvider>
                                            <Root
                                                cookies={cookies}
                                            />
                                        </IntlProvider>
                                    </Router>
                                </Provider>)
                            }
                        </Html>,
                    ),
                );
            }
        });
};

export default serverRenderer;
