import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import classnames from 'classnames';
import { Switch } from 'react-router';
import { connect } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import { renderRoutes } from 'react-router-config';
import { withTranslation } from 'react-i18next';
import { CookiesProvider, Cookies } from 'react-cookie';
import { compose, bindActionCreators } from 'redux';

import { Spinner } from './components';

import { setMode, setOrientation } from './actions/root';

import routes from './routes';
import { fonts } from './constants/style';
import { MOBILE_REGEXP, TABLET_REGEXP } from './constants/regExp';

import 'react-toastify/dist/ReactToastify.css';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import 'react-flags-select-ssr/css/react-flags-select.css';
import './Root.css';

import favicon from './assets/images/favicon/favicon.ico';

const mapStateToProps = state => ({ ...state.root });
const mapDispatchToProps = dispatch => bindActionCreators({
    setMode,
    setOrientation
}, dispatch);

@compose(
    withTranslation(),
    connect(mapStateToProps, mapDispatchToProps)
)

export class Root extends Component {
    componentDidMount() {
        this.changeHomeOverlay(this.props.overlay || this.props.loading || this.props.mobileMenu);
        window.addEventListener('resize', this.handleResize);
        this.handleResize();
    }

    shouldComponentUpdate(nextProps) {
        if (nextProps.overlay !== this.props.overlay || nextProps.loading !== this.props.loading || nextProps.mobileMenu !== this.props.mobileMenu) {
            this.changeHomeOverlay(nextProps.overlay || nextProps.loading || nextProps.mobileMenu);
        }

        return true;
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleResize);
    }

    handleResize = () => {
        const {
            mode,
            setMode,
            orientation,
            setOrientation
        } = this.props;

        let newMode = 'desktop';
        let newOrientation = 'portrait';

        if (TABLET_REGEXP.test(navigator.userAgent)) {
            newMode = 'tablet';
        } else if (MOBILE_REGEXP.test(navigator.userAgent)) {
            newMode = 'mobile';
        } else if (window.innerWidth < 768) {
            newMode = 'mobile';
        } else if (window.innerWidth < 992) {
            newMode = 'tablet';
        }

        if (window.innerWidth > window.innerHeight) {
            newOrientation = 'landscape';
        }

        if (mode !== newMode) {
            setMode(newMode);
        }

        if (orientation !== newOrientation) {
            setOrientation(newOrientation);
        }
    };

    changeHomeOverlay = overlay => {
        const homeHolder = document.getElementById('homeHolder');

        if (homeHolder) {
            homeHolder.style.overflow = overlay ? 'hidden' : 'visible';

            if (overlay) {
                this.scrollPosition = window ? window.scrollY : 0;

                homeHolder.style.cssText = `
				    top: -${this.scrollPosition}px;
				    left: 0px;
				    right: 0px;
					position: fixed;
				 `;
            } else {
                homeHolder.style.cssText = '';
                window.scroll(0, this.scrollPosition || 0);
                this.scrollPosition = 0;
            }
        }
    };

    render() {
        const { overlay, cookies, currentLanguage, t } = this.props;
        const cookieProviderProps = {};

        if (cookies) {
            cookieProviderProps.cookies = new Cookies(cookies);
        }

        return (
            <CookiesProvider
                {...cookieProviderProps}
            >
                <>
                    <Helmet>
                        <html
                            lang={currentLanguage}
                            prefix="og: https://ogp.me/ns#"
                        />
                        <meta
                            name="charSet"
                            content="utf-8"
                        />
                        <meta
                            name="theme-color"
                            content="#000000"
                        />
                        <meta
                            name="viewport"
                            content="width=device-width, initial-scale=1, user-scalable=yes"
                        />

                        <link
                            rel="icon"
                            type="image/png"
                            href={favicon}
                        />

                        <link
                            rel="preconnect"
                            href="https://fonts.gstatic.com/"
                            crossOrigin="anonymous"
                        />
                        <link
                            rel="preconnect"
                            href="https://connect.facebook.net/"
                            crossOrigin="anonymous"
                        />
                        <link
                            rel="preconnect"
                            href="https://www.googletagmanager.com/"
                            crossOrigin="anonymous"
                        />
                        <link
                            rel="preconnect"
                            href={process.env.BACKEND_URL}
                            crossOrigin="anonymous"
                        />
                        <title>{t('helmet-title')}</title>
                        <style type="text/css">
                            {fonts}
                        </style>
                    </Helmet>
                    <div
                        id="overlay"
                        className={classnames('spinnerHolder', { 'active': this.props.loading || overlay })}
                    >
                        {
                            this.props.loading && <Spinner play />
                        }
                    </div>
                    <ToastContainer
                        hideProgressBar
                        closeButton={() => false}
                    />
                    <div id="homeHolder">
                        <Switch>
                            {
                                renderRoutes(routes)
                            }
                        </Switch>
                    </div>
                </>
            </CookiesProvider>
        );
    }
}

export default Root;
