import React from 'react';
import {render} from '@testing-library/react';
import {Provider} from 'react-redux';
import {StaticRouter as Router} from 'react-router-dom';

import Component from './Root';
import IntlProvider from '../shared/i18n/IntlProvider';

import createHistory from './store/history';
import {configureStore} from './store';

const history = createHistory({initialEntries: []});
const store = configureStore({history});

describe('Root', () => {
    it('renders with no crash', () => {
        render(
            <Provider store={store}>
                <Router location={''} context={{}}>
                    <IntlProvider>
                        <Component />
                    </IntlProvider>
                </Router>
            </Provider>
        );
    });

    it('renders and matches snapshot', () => {
        const {container} = render(
            <Provider store={store}>
                <Router location={''} context={{}}>
                    <IntlProvider>
                        <Component />
                    </IntlProvider>
                </Router>
            </Provider>
        );

        expect(container).toMatchSnapshot();
    });
});
