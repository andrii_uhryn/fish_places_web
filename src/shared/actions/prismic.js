import Prismic from 'prismic-javascript';

import { setLoading } from './root';
import { handleError } from '../utils/fetchHelper';

import {
    SET_PRISMIC_CONTENT,
    SET_DEFAULT_PRISMIC_CONTENT
} from '../constants/actionTypes';

// GET

export const fetchPrismicContent = ({ type }) => (dispatch) => {
    dispatch(setLoading(true));

    Prismic
    .api(process.env.PRISMIC_API, { accessToken: process.env.PRISMIC_TOKEN })
    .then(api => {
        api
        .query(
            Prismic.Predicates.at(
                'document.tags',
                [type]
            ),
            { lang: 'en-gb' }
        )
        .then(res => {
            dispatch(setLoading(false));

            res && dispatch({ type: SET_PRISMIC_CONTENT, payload: res.results[0] });
        })
        .catch((error) => {
            dispatch(setLoading(false));
            handleError(error);
        });
    })
    .catch((error) => {
        dispatch(setLoading(false));
        handleError(error);
    });
};

export const cleanPrismic = () => ({ type: SET_DEFAULT_PRISMIC_CONTENT });
