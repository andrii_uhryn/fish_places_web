import * as TYPES from '../constants/actionTypes';

import { headers, handleError, fetchRequest } from '../utils/fetchHelper';

export const setFeedbacks = payload => ({ type: TYPES.SET_FEEDBACKS, payload });

// GET

export const fetchReviews = (_, cb) => dispatch => {
    return fetchRequest('/landing/feedback', {
        method: 'GET',
        headers,
    })
        .then(response => {
            dispatch(setFeedbacks(response));

            if (typeof cb === 'function') {
                cb();
            }
        })
        .catch(handleError);
};
