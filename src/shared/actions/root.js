import * as TYPES from '../constants/actionTypes';

export const setMode = payload => ({ type: TYPES.SET_MODE, payload });

export const setLoading = payload => ({ type: TYPES.SET_LOADING, payload: !!payload });

export const setMobileMenu = payload => ({ type: TYPES.SET_MOBILE_MENU, payload: !!payload });

export const cleanStore = () => ({ type: TYPES.CLEAR_STORE });

export const setBaseUrl = payload => ({ type: TYPES.SET_BASE_URL, payload });

export const setOverlay = payload => ({ type: TYPES.SET_OVERLAY, payload });

export const changeLanguage = payload => ({ type: TYPES.CHANGE_LANGUAGE, payload });

export const setOrientation = payload => ({ type: TYPES.SET_ORIENTATION, payload });

export const setScrollPosition = payload => ({ type: TYPES.SET_SCROLL_POSITION, payload });
