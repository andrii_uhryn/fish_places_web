import { fetchRequest, handleError, headers } from '../utils/fetchHelper';
import * as TYPES from '../constants/actionTypes';

export const setSupportSubmitting = payload => ({ type: TYPES.SET_SUPPORT_SUBMITTING, payload });

export const createSupport = (data, cb) => (dispatch) => {
    dispatch(setSupportSubmitting(true));

    fetchRequest('/feedbacks/create', {
        body: JSON.stringify(data),
        method: 'POST',
        headers,
    })
        .then(() => {
            typeof cb === 'function' && cb();
        })
        .catch(handleError)
        .finally(() => dispatch(setSupportSubmitting(false)));
};
