import React from 'react';

import './index.css';

export const BlurredTitle = (props) => {
    const { children, title } = props;

    return (
        <div className="container-holder">
            <div className="title-holder">
                <h1>
                    {title}
                </h1>
                <div className="blur-bg" />
            </div>
            <div className="bottom-content">
                {children}
            </div>
        </div>
    );
};

export default BlurredTitle;
