import React from 'react';
import {render, cleanup} from '@testing-library/react';

import Component from '.';

afterEach(() => {
    cleanup();
});

afterEach(() => {
    jest.clearAllMocks();
});

describe('BlurredTitle', () => {
    it('renders with title', () => {
        const title = 'test title';

        const {getByText} = render(<Component title={title} />);

        expect(getByText(title)).toBeInTheDocument();
    });

    it('renders with content', () => {
        const content = 'test content';

        const {getByText} = render(<Component>{content}</Component>);

        expect(getByText(content)).toBeInTheDocument();
    });

    it('renders with title and content', () => {
        const title = 'test title';
        const content = 'test content';

        const {getByText} = render(<Component title={title}>{content}</Component>);

        expect(getByText(title)).toBeInTheDocument();
        expect(getByText(content)).toBeInTheDocument();
    });

    it('renders and matches snapshot', () => {
        const title = 'test title';

        const {container} = render(<Component title={title} />);

        expect(container).toMatchSnapshot();
    });
});
