import React from 'react';
import cx from 'classnames';

import { Spinner, InlineSvg } from '../index';

import './index.css';

export const Button = (props) => {
    const { svg, loading, title, ariaLabel, color = '#EA990E', disabled, onClick, className, linkTo } = props;
    const handleClick = (e) => {
        typeof onClick === 'function' && onClick(e);

        if (linkTo) {
            window.open(linkTo, '_blank');
        }
    };

    return (
        <button
            onClick={handleClick}
            disabled={disabled}
            className={cx('button', className, color)}
            aria-label={ariaLabel || title}
        >
            {!!svg && <InlineSvg svg={svg} fill="#212328" />}
            {
                loading ? (
                    <Spinner
                        play
                        type='button'
                        width={50}
                        height={18}
                    />
                ) : title
            }
        </button>
    );
};

export default Button;
