import React from 'react';
import userEvent from '@testing-library/user-event';
import {render, cleanup} from '@testing-library/react';

import Component from '.';

const onClickMock = jest.fn();

afterEach(() => {
    cleanup();
});

afterEach(() => {
    jest.clearAllMocks();
});

describe('Button', () => {
    it('renders title when passed in', () => {
        const title = 'Click me';

        const {getByText} = render(<Component title={title} />);
        expect(getByText(title)).toBeInTheDocument();
    });

    it('renders className when passed in', () => {
        const title = 'Click me';
        const className = 'test-class-name';

        const {getByText} = render(<Component className={className} title={title} />);

        expect(getByText(title)).toHaveClass(className);
    });

    it('renders disabled button', () => {
        const title = 'Click me';

        const {container, getByText} = render(<Component disabled onClick={onClickMock}
                                                         title={title} />);

        expect(container.firstChild).toHaveAttribute('disabled');

        userEvent.click(getByText(title));

        expect(onClickMock).toHaveBeenCalledTimes(0);
    });

    it('renders button with click that works', () => {
        const title = 'Click me';

        const {getByText} = render(<Component onClick={onClickMock} title={title} />);

        userEvent.click(getByText(title));

        expect(onClickMock).toHaveBeenCalledTimes(1);
    });

    it('renders button with linkTo that works', () => {
        const title = 'Click me';
        const link = 'https://thefishplaces.com/en';

        const spy = jest.spyOn(window, 'open');

        const {getByText} = render(<Component linkTo={link} title={title} />);


        userEvent.click(getByText(title));

        expect(spy).toHaveBeenCalledWith(link, '_blank');
        expect(spy).toHaveBeenCalledTimes(1);
    });

    it('renders and matches snapshot', () => {
        const title = 'test title';

        const {container} = render(<Component title={title} />);

        expect(container).toMatchSnapshot();
    });
});
