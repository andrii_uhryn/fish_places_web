import React from 'react';
import Lottie from 'react-lottie';
import classNames from 'classnames';
import { connect } from 'react-redux';

import './index.css';

import tick from '../../assets/animations/tick.json';
import check from '../../assets/animations/checkbox.json';

const mapStateToProps = state => ({ language: state.root.currentLanguage });

@connect(mapStateToProps)

export class CustomCheck extends React.Component {
    handleClick = (e) => {
        typeof this.props.handleClick === 'function' && !this.props.disabled && this.props.handleClick(e);
    };

    render() {
        const {
            item,
            disabled,
            language,
            children,
        } = this.props;

        return (<div
            id={item.id}
            onClick={this.handleClick}
            className={classNames('custom-check-holder', {
                disabled,
                'mb-3' : item.marginBottom === 'mb-3',
                'flex-row': item.display === 'row',
                'flex-column': item.display === 'column',
                'defaultCursor' : item.cursor === 'default',
                'justify-content-start': item.display === 'row',
                'justify-content-between': item.display !== 'row'
            })}
            data-checked={!item.checked}
        >
            {
                item.checked ? <div className="animation-holder">
                    <Lottie
                        width={27}
                        height={27}
                        options={{
                            loop: false,
                            animationData: item.type === 'checkbox' ? check : tick,
                        }}
                    />
                </div> : <div className={classNames({
                    'empty-circle': item.type !== 'checkbox',
                    'empty-square': item.type === 'checkbox',
                })}/>
            }
            <div className={classNames({
                'label': true,
                'ml-2': item.display === 'row',
                'checked': item.checked,
                'text-left': item.display === 'row',
            })}>
                {
                    children ? children : item[`title_${language}`] || item[`answer_${language}`]
                }
            </div>
        </div>);
    }
}

export default CustomCheck;
