import React from 'react';
import cx from 'classnames';

import './index.css';

export const DropDown = (props) => {
    return (
        <div className="drop-down">

            <div className="dropdown-close" >
                    <p className="main-text-dropdown">
                        Choose country
                    </p>
                <div className="down-arrow" onClick={props.showContent}>
                    <i className={cx("fa fa-angle-down",{
                        'turn-arrow':props.dropDownState
                        })}aria-hidden="true"></i>
                </div>
            </div>
            {props.dropDownState &&

                <div className="list-item">
                    <ul className="country-list">
                            <li>
                                Country 1
                            </li>
                            <li>
                                Country 2
                            </li>
                            <li>
                                Country 3
                            </li>
                            <li>
                                Country 5
                            </li>
                            <li>
                                Country 6
                            </li>
                        </ul>
                </div>
            }
        </div>
    );
};

export default DropDown;
