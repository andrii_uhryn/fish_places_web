import React from 'react';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { useLocation } from 'react-router';

import { Button, InlineSvg } from '../index';

import { getLink } from '../../utils/urlHelper';
import { selectCurrentLanguage } from '../../selectors/root';

import { FB_LINK, INSTA_LINK, LINKED_IN_LINK } from '../../constants/general';

import './index.css';

import logo from '../../assets/images/logo/logo.svg';
import linkedinSVG from '../../assets/images/svg/linkedin.svg';
import facebookSVG from '../../assets/images/svg/facebook.svg';
import instagramSVG from '../../assets/images/svg/instagram.svg';

export const Footer = () => {
    const location = useLocation();
    const language = useSelector(selectCurrentLanguage);

    return (
        <footer>
            <div className="footer_section">
                <Link
                    to={getLink({
                        language,
                        location,
                        component: '/',
                        withQuery: true,
                        newSearch: '?scrollTo=main'
                    })}
                    className="footer_logo"
                >
                    <div className="logo_footer">
                        <InlineSvg svg={logo} fill="#212328"/>
                    </div>
                </Link>
            </div>
            <div className="social_ico">
                <div className="social_icon_space">
                    <Button
                        svg={linkedinSVG}
                        color="black"
                        linkTo={LINKED_IN_LINK}
                        ariaLabel="Linked In"
                    />
                </div>
                <div className="social_icon_space">
                    <Button
                        svg={instagramSVG}
                        color="black"
                        linkTo={INSTA_LINK}
                        ariaLabel="Instagram"
                    />
                </div>
                <div className="social_icon_space">
                    <Button
                        svg={facebookSVG}
                        color="black"
                        linkTo={FB_LINK}
                        ariaLabel="Facebook"
                    />
                </div>
            </div>

            <div className="rights">
                <p>Copyright © 2020</p>
            </div>
        </footer>

    );
};
export default Footer;
