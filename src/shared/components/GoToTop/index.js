import React from 'react';
import cx from 'classnames';
import { useSelector } from 'react-redux';
import { useHistory, useLocation } from 'react-router';

import { InlineSvg } from '../index';

import { getLink } from '../../utils/urlHelper';
import { selectCurrentLanguage, selectScrollPosition } from '../../selectors/root';

import goToTopIcon from '../../assets/images/svg/goToTop.svg';

import './index.css';

export const GoToTop = () => {
    const scrollPosition = useSelector(selectScrollPosition);
    const history = useHistory();
    const location = useLocation();
    const language = useSelector(selectCurrentLanguage);

    const handleClick = () => {
        history.push(getLink({
            language,
            location,
            component: '/',
            withQuery: true,
            newSearch: '?scrollTo=main'
        }));
    };

    return (
        <div
            onClick={handleClick}
            className={cx('go-to-top-holder', { show: scrollPosition !== 'main' })}
        >
            <InlineSvg svg={goToTopIcon} />
        </div>
    );
};

export default GoToTop;
