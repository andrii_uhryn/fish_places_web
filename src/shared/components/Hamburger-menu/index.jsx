import React from 'react';

import { InlineSvg } from '../index';

import menuSVG from '../../assets/images/svg/menu.svg';
import closeSVG from '../../assets/images/svg/close.svg';

import './index.css';

export const MenuBtn = (props) => {
    if (props.menuState) {
        return (
            <div className="close-btn" onClick={props.setMenuClose}>
                <InlineSvg svg={closeSVG} fill="#EA990E" />
            </div>
        );
    } else {
        return (
            <div className="burger-menu" onClick={props.setMenuOpen}>
                <InlineSvg svg={menuSVG} fill="#EA990E" />
            </div>
        );
    }
};

export default MenuBtn;
