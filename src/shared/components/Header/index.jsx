import React, { useState } from 'react';
import cx from 'classnames';
import { Link } from 'react-router-dom';
import ReactFlagsSelect from 'react-flags-select-ssr';
import { useTranslation } from 'react-i18next';
import { useHistory, useLocation } from 'react-router';
import { useSelector, useDispatch } from 'react-redux';

import { MenuBtn, Menu, InlineSvg } from '../../components';

import { getLink } from '../../utils/urlHelper';
import { changeLanguage, setMobileMenu } from '../../actions/root';
import { selectCurrentLanguage, selectScrollPosition } from '../../selectors/root';

import { LANGUAGES } from '../../constants/general';

import './index.css';

import logo from '../../assets/images/logo/logo.svg';

const nav = [
    {
        title: 'header-navigation-about',
        newSearch: '?scrollTo=about_us'
    },
    {
        title: 'header-navigation-screens',
        newSearch: '?scrollTo=screens'
    },
    {
        title: 'header-navigation-support',
        newSearch: '?scrollTo=support'
    }
];

export const Header = () => {
    const [t] = useTranslation();
    const history = useHistory();
    const location = useLocation();
    const dispatch = useDispatch();
    const language = useSelector(selectCurrentLanguage);
    const currentLanguage = LANGUAGES.filter((item) => item.id === language)[0];
    const [openMobileMenu, setOpenMobileMenu] = useState(false);
    const scrollPosition = useSelector(selectScrollPosition);

    const setMenuOpen = () => {
        dispatch(setMobileMenu(true));
        setOpenMobileMenu(true);
    };
    const setMenuClose = () => {
        dispatch(setMobileMenu(false));
        setOpenMobileMenu(false);
    };
    const handleLanguageChange = (newShortLabel) => {
        const newLanguage = LANGUAGES.filter((item) => item.shortLabel === newShortLabel)[0].id;

        dispatch(changeLanguage(newLanguage));
        history.push(
            getLink({
                location,
                language: newLanguage,
                withQuery: true
            })
        );
    };
    const renderFlagImage = (svg) => <InlineSvg svg={svg} className="language-selector" />;
    const getCustomLabels = () => {
        const labels = {};

        LANGUAGES.map((item) => {
            labels[item.shortLabel] = t(`change-language-label-${item.shortLabel}`);
        });

        return labels;
    };

    return (
        <header>
            <div className="main-header">
                <nav className="navigation-items">
                    <Link
                        to={getLink({
                            language,
                            location,
                            component: '/',
                            withQuery: true,
                            newSearch: '?scrollTo=main'
                        })}
                        className="logo"
                    >
                        <div className="Header-logo" onClick={setMenuClose}>
                            <InlineSvg svg={logo} fill="#EA990E" />
                        </div>
                    </Link>
                    <div className="left-section">
                        <div className="right-block">
                            <ReactFlagsSelect
                                showOptionLabel
                                showSelectedLabel
                                onSelect={handleLanguageChange}
                                customLabels={getCustomLabels()}
                                countries={LANGUAGES.map((item) => item.shortLabel)}
                                placeholder=""
                                defaultCountry={currentLanguage.shortLabel}
                                renderFlagImage={renderFlagImage}
                            />
                        </div>
                        <ul className="nav_bar">
                            {
                                nav.map(item => (
                                    <li
                                        key={item.title}
                                        className={cx({ active: item.newSearch === `?scrollTo=${scrollPosition}` })}
                                    >
                                        <Link
                                            to={getLink({
                                                language,
                                                location,
                                                component: '/',
                                                withQuery: true,
                                                newSearch: item.newSearch
                                            })}
                                        >
                                            {t(item.title)}
                                        </Link>
                                        <div className="line" />
                                    </li>
                                ))
                            }
                        </ul>
                    </div>
                    <div className="menu-block">
                        <MenuBtn
                            setMenuOpen={setMenuOpen}
                            setMenuClose={setMenuClose}
                            menuState={openMobileMenu}
                        />
                    </div>
                </nav>
            </div>
            <Menu menuState={openMobileMenu} setMenuClose={setMenuClose} />
        </header>
    );
};

export default Header;
