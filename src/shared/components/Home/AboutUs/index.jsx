import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { createPortal } from 'react-dom';
import { useTranslation } from 'react-i18next';

import { InlineSvg, Popup } from '../../index';

import { setOverlay } from '../../../actions/root';

import { ANDROID_LINK, IOS_LINK } from '../../../constants/general';

import chatSVG from '../../../assets/images/svg/chat.svg';
import settingsSVG from '../../../assets/images/svg/settings.svg';
import notificationSVG from '../../../assets/images/svg/notification.svg';
import twentyFourSevenSVG from '../../../assets/images/svg/24-hours.svg';

import './index.css';

export const AboutUs = () => {
    const [t] = useTranslation();
    const dispatch = useDispatch();
    const [renderPortal, setRenderPortal] = useState(false);

    const handleCloseModal = () => {
        dispatch(setOverlay(false));
        setRenderPortal(false);
    };
    const handleItemClick = () => {
        dispatch(setOverlay(true));
        setRenderPortal(true);
    };
    const handleAppStoreClick = () => {
        handleCloseModal();
        window.open(IOS_LINK, '_blank');
    };
    const handlePlayMarketClick = () => {
        handleCloseModal();
        window.open(ANDROID_LINK, '_blank');
    };

    return (
        <section className="section about-us">
            {
                renderPortal && createPortal(
                    <Popup
                        type="question"
                        title={t('question-join-title')}
                        subTitle={t('question-join-sub-title')}
                        closeModal={handleCloseModal}
                        buttons={[
                            {
                                color: 'blue',
                                title: t('getapp-button-1'),
                                onClick: handlePlayMarketClick,
                            },
                            {
                                color: 'blue',
                                title: t('getapp-button-2'),
                                onClick: handleAppStoreClick,
                            }
                        ]}
                    />,
                    document.getElementById('overlay'))
            }
            <div className="scroll_place" id="about_us" />
            <div className="text_headline_w content">
                <h2>{t('about-us-main-text')}</h2>
                <div className="line_w" />
                <h3>{t('about-us-sub-title')}</h3>
            </div>

            <div className="blocks content">
                <div className="block_title">
                    <div className="icons_rec">
                        <InlineSvg svg={settingsSVG} fill="#EA990E" />
                    </div>
                    <h3>{t('about-us-title-block-1')}</h3>
                    <p className="text_easy">
                        {t('about-us-text-block-1')}
                    </p>
                </div>

                <div className="block_title">
                    <div className="icons_rec">
                        <InlineSvg svg={twentyFourSevenSVG} fill="#EA990E" />
                    </div>
                    <h3>{t('about-us-title-block-2')}</h3>
                    <p className="text_easy">
                        {t('about-us-text-block-2')}
                    </p>
                </div>

                <div className="block_title">
                    <div className="icons_rec">
                        <InlineSvg svg={notificationSVG} fill="#EA990E" />
                    </div>
                    <h3>{t('about-us-title-block-3')}</h3>
                    <p className="text_easy">
                        {t('about-us-text-block-3')}
                    </p>
                </div>

                <div className="block_title">
                    <div className="icons_rec">
                        <InlineSvg svg={chatSVG} fill="#EA990E" />
                    </div>
                    <h3>{t('about-us-title-block-4')}</h3>
                    <p className="text_easy">
                        {t('about-us-text-block-4')}
                    </p>
                </div>
            </div>
            <div className="scroll_place" id="features_page" />
        </section>
    );
};
export default AboutUs;
