import React, { useState } from 'react';
import { createPortal } from 'react-dom';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { Button, Popup } from '../../';

import { setOverlay } from '../../../actions/root';
import { selectCurrentLanguage } from '../../../selectors/root';

import { ANDROID_LINK, IOS_LINK } from '../../../constants/general';

import mapEN from '../../../assets/images/banner/map_en.webp';
import mapUK from '../../../assets/images/banner/map_uk.webp';
import mapRU from '../../../assets/images/banner/map_ru.webp';
import fishGif from '../../../assets/images/banner/fish_animation.webp';

import './index.css';

const map = {
    en: mapEN,
    uk: mapUK,
    ru: mapRU
};

export const Banner = () => {
    const [t] = useTranslation();
    const currentLanguage = useSelector(selectCurrentLanguage);
    const dispatch = useDispatch();
    const [renderPortal, setRenderPortal] = useState(false);
    const handleAppStoreClick = () => {
        handleCloseModal();
        window.open(IOS_LINK, '_blank');
    };
    const handlePlayMarketClick = () => {
        handleCloseModal();
        window.open(ANDROID_LINK, '_blank');
    };
    const handleImageClick = () => {
        dispatch(setOverlay(true));
        setRenderPortal(true);
    };
    const handleCloseModal = () => {
        dispatch(setOverlay(false));
        setRenderPortal(false);
    };

    return (
        <section className="banner-holder" id="main">
            {
                renderPortal && createPortal(
                    <Popup
                        type="question"
                        title={t('question-join-title')}
                        subTitle={t('question-join-sub-title')}
                        closeModal={handleCloseModal}
                        buttons={[
                            {
                                color: 'blue',
                                title: t('getapp-button-1'),
                                onClick: handlePlayMarketClick,
                            },
                            {
                                color: 'blue',
                                title: t('getapp-button-2'),
                                onClick: handleAppStoreClick,
                            }
                        ]}
                    />,
                    document.getElementById('overlay'))
            }
            <div
                style={{ backgroundImage: `url(${fishGif})` }}
                className="content-holder-wrapper"
            >
                <div className="max-width-wrapper">
                <div className="title-wrapper">
                    <div className="banner-line" />
                    <h1>{t('banner-title')}</h1>
                    <div className="mobile-store-holder">
                        <Button
                            color="blue"
                            title={t('getapp-button-1')}
                            onClick={handlePlayMarketClick}
                        />
                        <Button
                            color="blue"
                            title={t('getapp-button-2')}
                            onClick={handleAppStoreClick}
                        />
                    </div>
                </div>
                <div className="map-image-holder">
                    <img
                        src={map[currentLanguage]}
                        alt="map image"
                        onClick={handleImageClick}
                        className="map-image"
                    />
                </div>
                </div>
            </div>
        </section>
    );
};

export default Banner;


