import React, { useState } from 'react';
import { createPortal } from 'react-dom';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { Button, Popup } from '../../';

import { setOverlay } from '../../../actions/root';
import { selectCurrentLanguage } from '../../../selectors/root';

import { ANDROID_LINK, IOS_LINK } from '../../../constants/general';

import './index.css';

const screens = {
    en: [
        require('../../../assets/images/mobile_app_screens/s1_en.webp').default,
        require('../../../assets/images/mobile_app_screens/s2_en.webp').default,
        require('../../../assets/images/mobile_app_screens/s3_en.webp').default,
        require('../../../assets/images/mobile_app_screens/s4_en.webp').default,
    ],
    uk: [
        require('../../../assets/images/mobile_app_screens/s1_uk.webp').default,
        require('../../../assets/images/mobile_app_screens/s2_uk.webp').default,
        require('../../../assets/images/mobile_app_screens/s3_uk.webp').default,
        require('../../../assets/images/mobile_app_screens/s4_uk.webp').default,
    ],
    ru: [
        require('../../../assets/images/mobile_app_screens/s1_ru.webp').default,
        require('../../../assets/images/mobile_app_screens/s2_ru.webp').default,
        require('../../../assets/images/mobile_app_screens/s3_ru.webp').default,
        require('../../../assets/images/mobile_app_screens/s4_ru.webp').default,
    ],
};

export const Screens = () => {
    const [t] = useTranslation();
    const language = useSelector(selectCurrentLanguage);
    const dispatch = useDispatch();
    const [renderPortal, setRenderPortal] = useState(false);

    const handleCloseModal = () => {
        dispatch(setOverlay(false));
        setRenderPortal(false);
    };
    const handleItemClick = () => {
        dispatch(setOverlay(true));
        setRenderPortal(true);
    };
    const handleAppStoreClick = () => {
        handleCloseModal();
        window.open(IOS_LINK, '_blank');
    };
    const handlePlayMarketClick = () => {
        handleCloseModal();
        window.open(ANDROID_LINK, '_blank');
    };

    return (
        <section className="section screens">
            {
                renderPortal && createPortal(
                    <Popup
                        type="question"
                        title={t('question-join-title')}
                        subTitle={t('question-join-sub-title')}
                        closeModal={handleCloseModal}
                        buttons={[
                            {
                                color: 'blue',
                                title: t('getapp-button-1'),
                                onClick: handlePlayMarketClick,
                            },
                            {
                                color: 'blue',
                                title: t('getapp-button-2'),
                                onClick: handleAppStoreClick,
                            }
                        ]}
                    />,
                    document.getElementById('overlay'))
            }
            <div className="scroll_place" id="screens" />
            <div className="text_headline_b">
                <h2>{t('screens-main-text')}</h2>
                <div className="line_b" />
                <h3>{t('screens-sub-title')}</h3>
            </div>
            <div className="screens-holder content">
                {screens[language].map((screen, index) => (
                    <div
                        key={`screen-${language}-index-${index}`}
                        onClick={handleItemClick}
                        className="image-wrapper"
                    >
                        <img src={screen} className="screen_photo" alt="photo" />
                    </div>
                ))}
            </div>
            <div className="buttons-holder">
                <Button
                    color="blue"
                    title={t('getapp-button-1')}
                    onClick={handlePlayMarketClick}
                />
                <Button
                    color="blue"
                    title={t('getapp-button-2')}
                    onClick={handleAppStoreClick}
                />
            </div>
        </section>
    );
};
export default Screens;
