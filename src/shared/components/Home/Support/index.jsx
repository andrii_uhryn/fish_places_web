import React, { useState } from 'react';
import { toast } from 'react-toastify';
import _debounce from 'lodash/debounce';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { Input, Textarea, Button, InlineSvg } from '../../index';

import { createSupport } from '../../../actions/support';
import { selectSubmitting } from '../../../selectors/support';

import { VALID_EMAIL } from '../../../constants/regExp';
import { EMAIL, PHONE_NUMBER } from '../../../constants/general';

import pinSVG from '../../../assets/images/svg/pin.svg';
import emailSVG from '../../../assets/images/svg/email.svg';
import phoneSVG from '../../../assets/images/svg/phone-call.svg';

import './index.css';

export const Support = () => {
    const [t] = useTranslation();
    const loading = useSelector(selectSubmitting);
    const dispatch = useDispatch();
    const [form, setForm] = useState({});
    const [errors, setErrors] = useState({});
    const disabled = !form.name || !form.email || !form.message || !!Object.keys(errors).length;

    const validateInput = _debounce((id, value) => {
        const newErrors = { ...errors };

        if (id === 'email') {
            if (!VALID_EMAIL.test(value)) {
                newErrors.email = t('email-error-invalid');
            } else {
                delete newErrors.email;
            }
        } else {
            if (!value) {
                newErrors[id] = t(`${id}-error-invalid`);
            } else {
                delete newErrors[id];
            }
        }

        delete newErrors.invalidCredentials;

        setErrors(newErrors);
    }, 1000);
    const inputChange = (e) => {
        const errors = { ...errors };

        delete errors[e.target.id];

        setForm({ ...form, [e.target.id]: e.target.value });
        setErrors(errors);
        validateInput(e.target.id, e.target.value);
    };
    const handleInputBlur = (e) => validateInput(e.target.id, e.target.value);
    const handleKeyPress = (e) => {
        if (e.keyCode === 13) {
            e.preventDefault();
            e.stopPropagation();

            return !disabled && onSubmit();
        }
    };

    const onSubmit = () => {
        const data = {
            name: form.name,
            email: form.email,
            reason: form.message
        };

        dispatch(createSupport(data, (error) => {
            if (!error) {
                toast(`${form.name}${t`support-success`}`, { className: 'success' });
                setForm({ name: '', email: '', message: '' });
            }
        }));
    };

    return (
        <section className="section support">
            <div className="scroll_place" id="support" />
            <div className="text_headline_w">
                <h2>{t('support-main-text')}</h2>
            </div>
            <div className="row_support">
                <div className="inputs">
                    <div className="inputItemHolder">
                        <Input
                            invalid={errors.name}
                            inputProps={{
                                id: 'name',
                                type: 'text',
                                value: form.name,
                                onBlur: handleInputBlur,
                                onChange: inputChange,
                                onKeyDown: handleKeyPress,
                                autoComplete: 'name'
                            }}
                            placeholder={t('support-input-1')}
                        />
                    </div>
                    <div className="inputItemHolder">
                        <Input
                            invalid={errors.email}
                            inputProps={{
                                id: 'email',
                                type: 'text',
                                value: form.email,
                                onBlur: handleInputBlur,
                                onChange: inputChange,
                                onKeyDown: handleKeyPress,
                                autoComplete: 'email'
                            }}
                            placeholder={t('support-input-2')}
                        />
                    </div>
                    <div className="textareaItemHolder">
                        <Textarea
                            invalid={errors.message}
                            textareaProps={{
                                id: 'message',
                                type: 'text',
                                value: form.message,
                                onBlur: handleInputBlur,
                                onChange: inputChange,
                                onKeyDown: handleKeyPress,
                                autoComplete: 'message'
                            }}
                            placeholder={t('support-textarea')}
                        />
                    </div>
                    <div className="buttons_space">
                        <Button
                            color="orange"
                            title={t('support-button')}
                            onClick={onSubmit}
                            loading={loading}
                            disabled={disabled}
                        />
                    </div>
                </div>
                <div className="contact_info">
                    <div className="blocks_supp">
                        <div className="supp_location_icon">
                            <InlineSvg svg={pinSVG} fill="#EA990E" />
                        </div>
                        <p className="supp">{t('support-contact-1')}</p>
                    </div>

                    <div className="blocks_supp">
                        <div className="supp_location_icon">
                            <InlineSvg svg={emailSVG} fill="#EA990E" />
                        </div>
                        <a href={`mailto:${EMAIL}`} className="supp">
                            {EMAIL}
                        </a>
                    </div>

                    <div className="blocks_supp">
                        <div className="supp_location_icon">
                            <InlineSvg svg={phoneSVG} fill="#EA990E" />
                        </div>
                        <p className="supp">{PHONE_NUMBER}</p>
                    </div>
                </div>
            </div>
        </section>
    );
};
export default Support;
