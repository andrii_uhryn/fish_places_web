import React, { useState } from 'react';
import cx from 'classnames';

import './index.css';

export const InlineSvg = props => {
    const {
        svg,
        fill = 'white',
        dataId,
        onClick,
        disabled,
        svgProps,
        className,
        hoveredFill,
        disabledFill,
    } = props;

    if (!svg) {
        return false;
    }

    const [hovered, setHovered] = useState(false);

    let fillColor;

    if (disabled && disabledFill) {
        fillColor = disabledFill;
    } else if (hoveredFill && hovered) {
        fillColor = hoveredFill;
    } else {
        fillColor = fill;
    }

    const __html = (svg.default || svg).replace('<svg ', `<svg fill="${fillColor}" `);
    const handleClick = (e) => {
        typeof onClick === 'function' && !disabled && onClick(e);
    };

    const onMouseEnter = () => {
        setHovered(true);
    };

    const onMouseLeave = () => {
        setHovered(false);
    };

    return <div
        {...svgProps}
        onClick={handleClick}
        data-id={dataId}
        className={cx('svgHolder', className, {
            cursorPointer: !disabled
        })}
        onMouseEnter={onMouseEnter}
        onMouseLeave={onMouseLeave}
        dangerouslySetInnerHTML={{ __html }}
    />;
};

export default InlineSvg;
