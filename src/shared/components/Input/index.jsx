import React, { useState, useEffect } from 'react';
import cx from 'classnames';

import { InlineSvg } from '../index';

import './index.css';

export const Input = props => {
    const {
        color,
        invalid,
        whiteBgc,
        iconProps = {},
        inputProps = {},
        loginInput,
        simpleInput,
        placeholder,
        desktopInput,
        noBorderBottom,
        preventLabelFloat,
        desktopErrorHolder,
        staticBackgroundIcon,
    } = props;
    let inputRef;
    const [focused, setFocused] = useState(!!inputProps.value);
    const renderIcon = (position) => {
        const icon = iconProps[position];

        if (!icon) {
            return false;
        }

        return (<InlineSvg
            {...icon}
            {...iconProps}
            className={`${position}-icon`}
        />);
    };
    const handleBlur = (e) => {
        if (!inputProps.value) {
            setFocused(false);
        }

        typeof inputProps.onBlur === 'function' && inputProps.onBlur(e);
    };
    const handleFocus = (e) => {
        setFocused(true);

        typeof inputProps.onFocus === 'function' && inputProps.onFocus(e);
    };
    const handleChange = (e) => {
        if (e.target.value) {
            setFocused(true);
        }

        typeof inputProps.onChange === 'function' && inputProps.onChange(e);
    };
    const handleRef = (ref) => {
        inputRef = ref;
        typeof inputProps.ref === 'function' && inputProps.ref(ref);
    };

    useEffect(() => {
        inputRef.addEventListener &&
        inputRef.addEventListener('animationstart', (e) => {
            if (e.animationName) {
                setFocused(true);
            }
        });
    }, []);

    useEffect(() => {
        if (inputProps.value) {
            setFocused(true);
        }
    }, [inputProps.value]);

    return (
        <div className={cx('inputHolder', color, {
            invalid: invalid && !desktopInput && !loginInput,
            success: !!inputProps.value && !desktopInput && !loginInput,
            noBorderBottom,
            preventLabelFloat
        })}>
            <div
                className={cx('inputWithIcon', {
                    desktopInput: desktopInput,
                    simpleInput: simpleInput,
                    loginInput: loginInput,
                    success: !!inputProps.value && !staticBackgroundIcon,
                    invalid,
                    whiteBgc,
                    disabledInput: inputProps.disabled,
                })}
            >
                {
                    !simpleInput && <div className={cx({
                        iconDivIcon: desktopInput,
                        success: !!inputProps.value && !staticBackgroundIcon,
                        invalid
                    })}>
                        {renderIcon('left')}
                    </div>
                }
                {
                    ((preventLabelFloat && !inputProps.value && !focused) || !preventLabelFloat) && (
                        <label
                            htmlFor={inputProps.id}
                            className={cx({
                                focused,
                                withLeftIcon: !!iconProps.left,
                                withRightIcon: !!iconProps.right
                            })}
                        >
                            {placeholder}
                        </label>
                    )
                }
                <input
                    {...inputProps}
                    ref={handleRef}
                    onBlur={handleBlur}
                    onFocus={handleFocus}
                    onChange={handleChange}
                />
                {renderIcon('right')}
                {
                    !!invalid && (
                        <div className={cx('errorHolder', {
                            desktopErrorHolder: desktopErrorHolder
                        })}>
                            {
                                invalid
                            }
                        </div>
                    )
                }
            </div>
        </div>
    );
};

export default Input;
