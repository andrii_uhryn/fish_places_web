import React from 'react';
import cx from 'classnames';
import { Link } from 'react-router-dom';
import { getLink } from '../../utils/urlHelper';
import ReactFlagsSelect from 'react-flags-select-ssr';
import { useTranslation } from 'react-i18next';
import { useLocation, useHistory } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';

import { InlineSvg } from '../';

import { changeLanguage } from '../../actions/root';
import { selectCurrentLanguage } from '../../selectors/root';

import { LANGUAGES } from '../../constants/general';

import './index.css';

const nav = [
    {
        title: 'header-navigation-about',
        newSearch: '?scrollTo=about_us'
    },
    {
        title: 'header-navigation-screens',
        newSearch: '?scrollTo=screens'
    },
    {
        title: 'header-navigation-support',
        newSearch: '?scrollTo=support'
    },
];

export const Menu = (props) => {
    const [t] = useTranslation();
    const history = useHistory();
    const location = useLocation();
    const dispatch = useDispatch();
    const language = useSelector(selectCurrentLanguage);
    const currentLanguage = LANGUAGES.filter((item) => item.id === language)[0];

    const handleLanguageChange = (newShortLabel) => {
        const newLanguage = LANGUAGES.filter((item) => item.shortLabel === newShortLabel)[0].id;

        dispatch(changeLanguage(newLanguage));
        history.push(
            getLink({
                location,
                language: newLanguage,
                withQuery: true,
            })
        );
        props.setMenuClose();
    };
    const renderFlagImage = (svg) => <InlineSvg svg={svg} className="language-selector" />;

    return (
        <div className={cx('menu', { menuOpen: props.menuState })}>
            <div className="top-block">
                <ReactFlagsSelect
                    showOptionLabel
                    showSelectedLabel
                    onSelect={handleLanguageChange}
                    countries={LANGUAGES.map((item) => item.shortLabel)}
                    placeholder=""
                    defaultCountry={currentLanguage.shortLabel}
                    renderFlagImage={renderFlagImage}
                />
                <ul className="menu-mobile">
                    {
                        nav.map(item => (
                            <li key={item.title} onClick={props.setMenuClose}>
                                <Link
                                    to={getLink({
                                        language,
                                        location,
                                        component: '/',
                                        withQuery: true,
                                        newSearch: item.newSearch
                                    })}
                                >
                                    {t(item.title)}
                                </Link>
                            </li>
                        ))
                    }
                </ul>
            </div>
        </div>
    );
};

export default Menu;
