import React, { PureComponent } from 'react';
import { Helmet } from 'react-helmet';

import { formatStringLength } from '../../utils/string';
import { SEO_CONTENT_LENGTH } from '../../constants/general';

export class Meta extends PureComponent {
	render() {
		const {
			url,
			about,
			image,
			title,
			actor,
			robots,
			endDate,
			xDefault,
			language,
			alternate,
			organiser,
			startDate,
			description,
			googleVerification,
		} = this.props;
		
		return <Helmet>
			{
				title && <title>
					{
						formatStringLength(title, SEO_CONTENT_LENGTH.title)
					}
				</title>
			}
			{
				alternate && alternate.map((item, index) => <link
					rel="alternate"
					key={`alternate-link-${index}`}
					href={item.url}
					hrefLang={item.language}
				/>)
			}
			{
				xDefault && <link
					rel="alternate"
					href={xDefault}
					hrefLang="x-default"
				/>
			}
			{
				url && <link
					rel="canonical"
					href={url}
				/>
			}
			
			// Google +
			
			{
				description && <meta
					name="description"
					content={formatStringLength(description, SEO_CONTENT_LENGTH.description)}
				/>
			}
			{
				googleVerification && <meta
					name="google-site-verification"
					content={googleVerification}
				/>
			}
			{
				language && <meta
					name="language"
					content={language}
				/>
			}
			{
				robots && <meta
					name="robots"
					content="index,follow"
				/>
			}
			{
				about && <meta
					name="about"
					content={about}
				/>
			}
			{
				actor && <meta
					name="actor"
					content={actor}
				/>
			}
			{
				organiser && <meta
					name="organiser"
					content={organiser}
				/>
			}
			{
				endDate && <meta
					name="EndDate"
					content={endDate}
				/>
			}
			{
				startDate && <meta
					name="StartDate"
					content={startDate}
				/>
			}
			
			// OG
			
			{
				title && <meta
					property="og:title"
					content={formatStringLength(title, SEO_CONTENT_LENGTH.title)}
				/>
			}
			{
				image && <meta
					property="og:image"
					content={image}
				/>
			}
			{
				description && <meta
					property="og:description"
					content={formatStringLength(description, SEO_CONTENT_LENGTH.description)}
				/>
			}
			{
				url && <meta
					property="og:url"
					content={url}
				/>
			}
		</Helmet>;
	}
}

export default Meta;
