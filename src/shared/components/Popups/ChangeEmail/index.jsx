import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

import Button from '../../Button';

import './index.css';
import { Input } from '../../Input';
import _debounce from 'lodash/debounce';
import { VALID_EMAIL } from '../../../constants/regExp';

export const ChangeEmail = (props) => {
    const { onSubmit, closeModal } = props;
    const [t] = useTranslation();
    const [email, setEmail] = useState('');
    const [errors, setErrors] = useState({});
    const disabled = !email || !!Object.keys(errors).length;

    const validateInput = _debounce((id, value) => {
        const newErrors = { ...errors };

        if (id === 'email') {
            if (!VALID_EMAIL.test(value)) {
                newErrors.email = t('email-error-invalid');
            } else {
                delete newErrors.email;
            }
        }

        setErrors(newErrors);
    }, 1000);
    const inputChange = (e) => {
        const errors = { ...errors };

        delete errors[e.target.id];

        setEmail(e.target.value);
        setErrors(errors);
        validateInput(e.target.id, e.target.value);
    };
    const handleInputBlur = (e) => validateInput(e.target.id, e.target.value);
    const handleKeyPress = (e) => {
        if (e.keyCode === 13) {
            e.preventDefault();
            e.stopPropagation();

            return !disabled && onSubmit(email);
        }
    };


    return (
        <div className="changeEmailPopup">
            <div className="title">
                {t('popup-change-email-title')}
            </div>
            <div className="subTitle">
                {t('popup-change-email-subtitle')}
            </div>
            <div className="input-holder">
                <Input
                    color="blue"
                    invalid={errors.email}
                    inputProps={{
                        id: 'email',
                        type: 'email',
                        value: email,
                        onBlur: handleInputBlur,
                        onChange: inputChange,
                        onKeyDown: handleKeyPress
                    }}
                    placeholder={t('sign-in-placeholder-1')}
                />
            </div>
            <div className="button-container">
                <Button
                    title={t('button-send')}
                    onClick={() => onSubmit(email)}
                    disabled={disabled}
                />
                <Button
                    color="blue"
                    title={t('button-close')}
                    onClick={closeModal}
                />
            </div>
        </div>
    );
};

export default ChangeEmail;
