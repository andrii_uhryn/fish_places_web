import React from 'react';
import { useTranslation } from 'react-i18next';

import Button from '../../Button';

import './index.css';

export const Info = (props) => {
    const { title, subTitle, closeModal } = props;
    const [t] = useTranslation();

    return (
        <div className="infoPopup">
            <div className="title">
                {title}
            </div>
            <div className="subTitle">
                {subTitle}
            </div>
            <div className="button-container">
                <div className="button-space right">
                    <Button
                        title={t('close-button-title')}
                        onClick={closeModal}
                    />
                </div>
            </div>
        </div>
    );
};

export default Info;
