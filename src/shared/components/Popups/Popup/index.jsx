import React, { useEffect } from 'react';
import OutsideClickHandler from 'react-outside-click-handler';

import { InlineSvg } from '../../index';

import closeSVG from '../../../assets/images/svg/close.svg';

import './index.css';

const children = {
    info: require('../Info/index').default,
    question: require('../Question/index').default,
    changeEmail: require('../ChangeEmail/index').default
};

export const Popup = props => {
    const { type, closeModal } = props;
    const Child = children[type];
    const handleKeyDown = (e) => {
        if (e.keyCode === 27) {
            closeModal();
        }
    };

    useEffect(() => {
        document.addEventListener('keydown', handleKeyDown, false);

        return () => {
            document.removeEventListener('keydown', handleKeyDown, false);
        };
    }, []);

    return (
        <OutsideClickHandler onOutsideClick={closeModal}>
            <div className="popupWrapper">
                <div className="close-icon" onClick={closeModal}>
                    <InlineSvg svg={closeSVG} fill="#212328" />
                </div>
                <Child {...props} />
            </div>
        </OutsideClickHandler>
    );
};

export default Popup;
