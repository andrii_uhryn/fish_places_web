import React from 'react';

import Button from '../../Button';

import './index.css';

export const Question = (props) => {
    const { title, subTitle, buttons } = props;

    return (
        <div className="questionPopup">
            <div className="title">
                {title}
            </div>
            <div className="subTitle">
                {subTitle}
            </div>
            <div className="button-container">
                {buttons.map((button, index) => (
                    <div className="button-space" key={`button-${index}`}>
                        <Button
                            {...button}
                        />
                    </div>
                ))}
            </div>
        </div>
    );
};

export default Question;
