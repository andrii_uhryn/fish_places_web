import React from 'react';
import cx from 'classnames';

import './index.css';

export const Arrow = (props) => {
    const { onClick,arrowType,arrowColor } = props;

    return (
       <div className={cx("arrow", arrowType)} onClick={onClick}>
           <i className={cx('fa',{
               'fa-angle-left': arrowType === 'Back',
               'fa-angle-right': arrowType === 'Next'
                })} aria-hidden="true" style={{color:arrowColor}}></i>
       </div>

    );

}
export default Arrow;
