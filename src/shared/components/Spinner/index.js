import React from 'react';
import Lottie from 'react-lottie';

import loader from '../../assets/animations/loader_black.json';
import spinner from '../../assets/animations/spinner.json';

export const Spinner = props => (<Lottie
    width={props.width ? props.width : 150}
    height={props.height ? props.height : 150}
    options={{
        loop: true,
        autoplay: props.play,
        animationData: props.type === 'button' ? (loader) : (spinner) ,
    }}
/>);

export default Spinner;
