import React, { useState, useEffect } from 'react';
import cx from 'classnames';

import { InlineSvg } from '../index';

import './index.css';

export const Textarea = props => {
    const {
        invalid,
        whiteBgc,
        iconProps = {},
        placeholder,
        textareaProps = {},
        noBorderBottom,
        preventLabelFloat,
        desktopErrorHolder,
        staticBackgroundIcon,
    } = props;
    let textareaRef;
    const [focused, setFocused] = useState(!!textareaProps.value);
    const renderIcon = (position) => {
        const icon = iconProps[position];

        if (!icon) {
            return false;
        }

        return (<InlineSvg
            {...icon}
            {...iconProps}
            className={`${position}-icon`}
        />);
    };
    const handleBlur = (e) => {
        if (!textareaProps.value) {
            setFocused(false);
        }

        typeof textareaProps.onBlur === 'function' && textareaProps.onBlur(e);
    };
    const handleFocus = (e) => {
        setFocused(true);

        typeof textareaProps.onFocus === 'function' && textareaProps.onFocus(e);
    };
    const handleChange = (e) => {
        if (e.target.value) {
            setFocused(true);
        }

        typeof textareaProps.onChange === 'function' && textareaProps.onChange(e);
    };
    const handleRef = (ref) => {
        textareaRef = ref;
        typeof textareaProps.ref === 'function' && textareaProps.ref(ref);
    };

    useEffect(() => {
        textareaRef.addEventListener &&
        textareaRef.addEventListener('animationstart', (e) => {
            if (e.animationName) {
                setFocused(true);
            }
        });
    }, []);

    useEffect(() => {
        if (textareaProps.value) {
            setFocused(true);
        }
    }, [textareaProps.value]);


    return (
        <div className={cx('textareaHolder', {
            invalid: invalid,
            success: !!textareaProps.value,
            noBorderBottom,
            preventLabelFloat
        })}>
            <div
                className={cx('textareaWithIcon', {
                    success: !!textareaProps.value && !staticBackgroundIcon,
                    invalid,
                    whiteBgc,
                    disabledTextarea: textareaProps.disabled,
                })}
            >
                <div className={cx({
                    success: !!textareaProps.value && !staticBackgroundIcon,
                    invalid
                })}>
                    {renderIcon('left')}
                </div>
                {
                    ((preventLabelFloat && !textareaProps.value && !focused) || !preventLabelFloat) && (
                        <label
                            htmlFor={textareaProps.id}
                            className={cx({
                                focused,
                                withLeftIcon: !!iconProps.left,
                                withRightIcon: !!iconProps.right
                            })}
                        >
                            {placeholder}
                        </label>
                    )
                }
                <textarea
                    {...textareaProps}
                    ref={handleRef}
                    onBlur={handleBlur}
                    onFocus={handleFocus}
                    onChange={handleChange}
                />
                {renderIcon('right')}
                {
                    !!invalid && (
                        <div className={cx('errorHolder', {
                            desktopErrorHolder: desktopErrorHolder
                        })}>
                            {
                                invalid
                            }
                        </div>
                    )
                }
            </div>
        </div>
    );
};

export default Textarea;
