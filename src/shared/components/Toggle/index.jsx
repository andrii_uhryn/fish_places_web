import React from 'react';

import './index.css';

export const Toggle = (props) => {
    const { id, checked } = props;

    const onChange = () => {
        typeof props.onChange === 'function' && props.onChange(id, !checked);
    };

    return (
        <div className="toggle-space">
            <div
                onClick={onChange}
                className={checked ? 'toggle-active' : 'toggle-inactive'}
            >
                <div
                    className={checked ? 'inside-switcher-active' : 'inside-switcher-inactive'}
                />
            </div>
            <p className="toggle-title-active">
                Mobile notifications
            </p>
        </div>
    );
};

export default Toggle;
