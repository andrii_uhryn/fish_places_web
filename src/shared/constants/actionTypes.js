// root
export const SET_MODE = 'SET_MODE';
export const SET_ERROR = 'SET_ERROR';
export const SET_THEME = 'SET_THEME';
export const SET_OVERLAY = 'SET_OVERLAY';
export const SET_LOADING = 'SET_LOADING';
export const CLEAR_THEME = 'CLEAR_THEME';
export const CLEAR_STORE = 'CLEAR_STORE';
export const SET_BASE_URL = 'SET_BASE_URL';
export const SET_ORIENTATION = 'SET_ORIENTATION';
export const CHANGE_LANGUAGE = 'CHANGE_LANGUAGE';
export const SET_MOBILE_MENU = 'SET_MOBILE_MENU';
export const SET_SCROLL_POSITION = 'SET_SCROLL_POSITION';

// support
export const SET_SUPPORT_SUBMITTING = 'SET_SUPPORT_SUBMITTING';

// feedback
export const SET_FEEDBACKS = "SET_FEEDBACKS";

// prismic
export const SET_PRISMIC_CONTENT = 'SET_PRISMIC_CONTENT';
export const SET_DEFAULT_PRISMIC_CONTENT = 'SET_DEFAULT_PRISMIC_CONTENT';
