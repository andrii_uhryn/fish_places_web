export const LANGUAGES = [
    {
        id: 'en',
        label: 'English',
        shortLabel: 'GB',
    },
    {
        id: 'uk',
        label: 'Ukrainian',
        shortLabel: 'UA',
    },
    {
        id: 'ru',
        label: 'Russian',
        shortLabel: 'RU',
    },
];
export const DEFAULT_LANGUAGE = 'en';

export const URL_LENGTH = {
    default: 20,
    title: 50,
};

export const SEO_CONTENT_LENGTH = {
    default: 57,
    title: 57,
    description: 157,
};

export const SCALE_MAX = 2;
export const SCALE_STEP = 0.5;

export const UPLOAD_FILE_TYPES = '.pdf,.jpeg,.jpg,.png,.doc,.docx,.pages,.xlsx,.xlsm,.xltx,.xltm';

export const EMAIL = 'admin@thefishplaces.com'
export const PHONE_NUMBER = '+380990079103'

export const IOS_LINK = 'https://apps.apple.com/us/app/fish-places/id1503903783';
export const ANDROID_LINK = 'https://play.google.com/store/apps/details?id=com.fishplaces';

export const FB_LINK = 'https://www.facebook.com/thefishplaces';
export const INSTA_LINK = 'https://www.instagram.com/thefishplaces';
export const LINKED_IN_LINK = 'https://www.linkedin.com/company/fish-places';

