export const YOU_TUBE_VALID_URL = /((http(s)?:\/\/)?)(www\.)?((youtube\.com\/)|(youtu.be\/))[\S]+/;
export const CUT_YOU_TUBE_ID = /^.*(?:(?:youtu\.be\/|v\/|vi\/|u\/\w\/|embed\/)|(?:(?:watch)?\?v(?:i)?=|&v(?:i)?=))([^#&?]*).*/;

export const VIMEO_VALID_URL = /(?:https?:\/\/)?(?:www\.)?(?:vimeo\.com\/)([0-9]+)/;

export const VALID_EMAIL = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
export const VALID_NAME = /^[a-zA-Z][a-zA-Z\s]*$/;
export const VALID_PHONE_NUMBER = /[+][0-9]/;
export const PASSWORD_DIGIT = /\d/;
export const PASSWORD_UPPER = /[A-Z]/;

export const COMMA_REGEXP = /(\d+)(\d{3})/;

export const TABLET_REGEXP = /Tablet|iPad/i;
export const MOBILE_REGEXP = /Android|iPhone|Mobile Safari|IEMobile|Windows Phone|Lumia|BlackBerry|PlayBook|BB10|webOS|Mobile|Tablet|Opera Mini|\bCrMo\/|Opera Mobi/i;
