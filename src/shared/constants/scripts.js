import React from 'react';

export const googleTagManager = <script dangerouslySetInnerHTML={{
    __html: `
        (function(w,d,s,l,i){
			w[l]=w[l]||[];
			w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});
			var f=d.getElementsByTagName(s)[0],
				j=d.createElement(s),
				dl=l!='dataLayer'?'&l='+l:'';

			j.async=true;
			j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;
			f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','${process.env.GTM_ID}')
`,
}}
/>;

export const googleTagNoScript = <noscript dangerouslySetInnerHTML={{
    __html: `
        <iframe src="https://www.googletagmanager.com/ns.html?id=${process.env.GTM_ID}"
            height="0"
            width="0"
            style="display: none;visibility: hidden"
        />
    `
}} />;

export const fbScript = <script dangerouslySetInnerHTML={{
    __html: `
        window.fbAsyncInit = function() {
            FB.init({
                appId                : '${process.env.FB_ID}',
                autoLogAppEvents     : true,
                xfbml                : true,
                version              : 'v5.0'
            });
          };

        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    `,
}}
/>;

export const cloudinaryWidget = <script
    async
    src="https://widget.cloudinary.com/v2.0/global/all.js"
    type="text/javascript"
/>;

export const liqpay = <script
    async
    src="https://static.liqpay.ua/libjs/checkout.js"
    type="text/javascript"
/>;

export const telegramWidget = <script
    async
    src="https://telegram.org/js/telegram-widget.js"
    type="text/javascript"
/>;
