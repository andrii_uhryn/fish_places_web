import React, { useEffect } from 'react';
import _throttle from 'lodash/throttle';
import queryString from 'query-string';
import { renderRoutes } from 'react-router-config';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { Meta, Header, Footer, GoToTop } from '../../components';
import { Banner, Support, Screens, AboutUs } from '../../components/Home';

import { getLink } from '../../utils/urlHelper';
import { smoothScroll } from '../../utils/smoothScroll';
import { getTranslated } from '../../../server/middleware/generateSiteMap';
import { setScrollPosition } from '../../actions/root';
import { selectBaseURL, selectMode, selectCurrentLanguage } from '../../selectors/root';

import { DEFAULT_LANGUAGE } from '../../constants/general';

import './index.css';

import metaImage from '../../assets/images/meta/home.webp';

export const Home = props => {
    const { route, match, history, location } = props;
    const [t] = useTranslation();
    const dispatch = useDispatch();
    const mode = useSelector(selectMode);
    const baseURL = useSelector(selectBaseURL);
    const language = useSelector(selectCurrentLanguage);
    const { scrollTo } = queryString.parse(location.search);

    const getPosition = (key) => {
        const headerDiff = mode === 'mobile' ? 30 : 0;
        return +(document.getElementById(key).getBoundingClientRect().top + window.pageYOffset).toFixed(0) + headerDiff;
    };

    const handleScroll = _throttle(() => {
        const scrollYPosition = window.scrollY || window.pageYOffset || document.documentElement.scrollTop;

        let newActiveTab = 'main';

        if(scrollYPosition >= getPosition('about_us') && scrollYPosition < getPosition('screens')) {
            newActiveTab = 'about_us';
        } else if(scrollYPosition >= getPosition('screens') && scrollYPosition < getPosition('support')) {
            newActiveTab = 'screens';
        } else if(scrollYPosition >= getPosition('support')) {
            newActiveTab = 'support';
        }

        dispatch(setScrollPosition(newActiveTab));
    }, 500);

    useEffect(() => {
        if (match.isExact) {
            setTimeout(() => {
                smoothScroll.scrollToElementById(scrollTo, { mode }, () => {
                    history.replace(getLink({
                        location,
                        language,
                        component: '/'
                    }));
                });
            }, 0);
        }
    }, [scrollTo]);

    useEffect(() => {
        if (!match.isExact) {
            setTimeout(() => {
                window.scroll(0, 0);
            }, 0);
            dispatch(setScrollPosition('main'));
        }
    }, [location.pathname]);

    useEffect(() => {
        window.addEventListener('scroll', handleScroll);

        return () => {
            window.removeEventListener('scroll', handleScroll);
        };
    }, []);

    return (
        <div className="home">
            <Header />
            {
                match.isExact ? (<>
                    <Meta
                        url={`${baseURL}${location.pathname}`}
                        title={t('helmet-title')}
                        image={metaImage}
                        robots={process.env.NODE_ENV_PRIMARY === 'production'}
                        language={language.toUpperCase()}
                        xDefault={`${baseURL}${DEFAULT_LANGUAGE === language ? location.pathname : getLink({
                            language: DEFAULT_LANGUAGE,
                            location: { pathname: location.pathname }
                        }).pathname}`}
                        alternate={getTranslated(location.pathname).map(item => ({
                            ...item,
                            url: `${baseURL}${item.url}`
                        }))}
                        description={t('about-us-sub-title')}
                        googleVerification={process.env.GOOGLE_BOT_VERIFICATION}
                    />
                    <Banner />
                    <AboutUs />
                    <Screens />
                    <Support />
                    <GoToTop />
                </>) : renderRoutes(route.routes)
            }
            <Footer />
        </div>
    );
};

export default Home;
