import React, { Component } from 'react';
import { connect } from 'react-redux';
import { RichText } from 'prismic-reactjs';
import { compose, bindActionCreators } from 'redux';

import { BlurredTitle } from '../../../components';

import { selectCurrentLanguage } from '../../../selectors/root';
import { cleanPrismic, fetchPrismicContent } from '../../../actions/prismic';

import './index.css';

const mapStateToProps = state => ({
    ...state.prismic,
    currentLanguage: selectCurrentLanguage(state)
});
const mapDispatchToProps = dispatch => bindActionCreators({
    cleanPrismic,
    fetchPrismicContent
}, dispatch);

@compose(
    connect(mapStateToProps, mapDispatchToProps)
)

export class TermsAndConditions extends Component {
    componentDidMount() {
        this.fetchData();
    }

    componentWillUnmount() {
        this.props.cleanPrismic();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.currentLanguage !== this.props.currentLanguage) {
            this.fetchData();
        }
    }

    fetchData = () => {
        const { currentLanguage, fetchPrismicContent } = this.props;

        fetchPrismicContent({ type: `terms_${currentLanguage}` });
    };

    render() {
        const { data } = this.props;

        return (
            <div className="terms">
                <BlurredTitle
                    title={RichText.asText(data.title)}
                >
                    <RichText
                        render={data.content}
                    />
                </BlurredTitle>
            </div>
        );
    }
}

export default TermsAndConditions;
