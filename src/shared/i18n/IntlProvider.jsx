import React, { useEffect } from 'react';
import i18next from 'i18next';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { I18nextProvider } from 'react-i18next';

import { LANGUAGES, DEFAULT_LANGUAGE } from '../constants/general';

const resources = {};
const languages = LANGUAGES.map(item => {
    resources[item.id] = require(`./locales/${item.id}.json`);

    return item.id;
});

i18next.init({
    resources,
    fallbackLng: DEFAULT_LANGUAGE,
    fallbackNS: ['translation'],
    parseMissingKeyHandler: (missing) => {
        if (process.env.NODE_ENV === 'development') {
            console.warn('MISSING TRANSLATION:', missing);
        }
        return missing;
    },
});

i18next.languages = languages;

const I18N = ({ children, locale }) => {
    if (i18next.language !== locale) {
        i18next.changeLanguage(locale);
    }

    return <I18nextProvider
        i18n={i18next}
    >
        {
            children
        }
    </I18nextProvider>;
};

I18N.propTypes = {
    children: PropTypes.any,
    locale: PropTypes.oneOf(languages),
};

const mapStateToProps = (state) => ({ locale: state.root.currentLanguage });

export default connect(mapStateToProps, null, null, { pure: false })(I18N);
