import en from './en';
import uk from './uk';
import ru from './ru';

export default {
    en: en.translation,
    uk: uk.translation,
    ru: ru.translation,
};
