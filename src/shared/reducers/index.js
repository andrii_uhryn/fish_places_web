import { connectRouter } from 'connected-react-router';
import { combineReducers } from 'redux';

import root from './root';
import prismic from './prismic';
import support from './support';
import reviews from './reviews';

const createRootReducer = (history) =>
    combineReducers({
        root,
        prismic,
        reviews,
        support,
        router: connectRouter(history)
    });

export default createRootReducer;
