import {
    CLEAR_STORE,
    SET_PRISMIC_CONTENT,
    SET_DEFAULT_PRISMIC_CONTENT
} from '../constants/actionTypes';

const initialState = {
    data: {
        title: [],
        content: []
    }
};

export default (state = initialState, { type, payload }) => {
    switch (type) {
        case SET_PRISMIC_CONTENT:
            return {
                ...state,
                ...payload
            };

        case CLEAR_STORE:
        case SET_DEFAULT_PRISMIC_CONTENT:
            return {
                ...initialState
            };

        default:
            return state;
    }
};
