import * as TYPES from '../constants/actionTypes';

const initialState = {
    collection: {
        results: [],
    },
};

export default (state = initialState, { type, payload }) => {
    switch (type) {
        case TYPES.SET_FEEDBACKS:
            return {
                ...state,
                collection: {
                    ...payload,
                }
            };

        case TYPES.CLEAR_STORE:
            return {
                ...initialState,
            };

        default:
            return state;
    }
};
