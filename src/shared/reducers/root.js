import * as TYPES from '../constants/actionTypes';
import { DEFAULT_LANGUAGE } from '../constants/general';

const initialState = {
    mode: 'desktop',
    error: null,
    baseURL: '',
    overlay: false,
    loading: false,
    themeMode: 'defaultTheme',
    mobileMenu: false,
    orientation: 'portrait',
    changedFields: {},
    scrollPosition: 'main',
    currentLanguage: DEFAULT_LANGUAGE,
};

export default (state = initialState, { type, payload }) => {
    switch (type) {
        case TYPES.SET_THEME:
            return {
                ...state,
                themeMode: payload,
            };

        case TYPES.SET_SCROLL_POSITION:
            return {
                ...state,
                scrollPosition: payload,
            };

        case TYPES.SET_MOBILE_MENU:
            return {
                ...state,
                mobileMenu: payload,
            };

        case TYPES.SET_OVERLAY:
            return {
                ...state,
                overlay: payload,
            };

        case TYPES.SET_ERROR:
            return {
                ...state,
                error: payload,
            };

        case TYPES.SET_LOADING:
            return {
                ...state,
                loading: payload,
            };

        case TYPES.CLEAR_THEME:
            return {
                ...state,
                themeMode: initialState.themeMode,
            };

        case TYPES.SET_MODE:
            return {
                ...state,
                mode: payload,
            };

        case TYPES.SET_BASE_URL:
            return {
                ...state,
                baseURL: payload,
            };

        case TYPES.SET_ORIENTATION:
            return {
                ...state,
                orientation: payload,
            };

        case TYPES.CHANGE_LANGUAGE:
            return {
                ...state,
                currentLanguage: payload,
            };

        case TYPES.CLEAR_STORE:
            return {
                ...initialState,
                currentLanguage: state.currentLanguage,
            };

        default:
            return state;
    }
};
