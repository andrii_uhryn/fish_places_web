import * as TYPES from '../constants/actionTypes';

const initialState = {
    submitting: false,
};

export default (state = initialState, { type, payload }) => {
    switch (type) {
        case TYPES.SET_SUPPORT_SUBMITTING:
            return {
                ...state,
                submitting: payload,
            };

        case TYPES.CLEAR_STORE:
            return {
                ...initialState,
            };

        default:
            return state;
    }
};
