import {
    Home,
    PrivacyPolicy,
    TermsAndConditions
} from './containers';

import { getUrlRegExp } from './utils/urlHelper';

import { LANGUAGES } from './constants/general';

const homeUrl = `/(${LANGUAGES.map(item => item.id).join('|')})`;

const routes = [
    {
        path: homeUrl,
        component: Home,
        routes: [
            {
                path: `${homeUrl}/${getUrlRegExp('privacy')}`,
                exact: true,
                disallow: true,
                component: PrivacyPolicy,
            },
            {
                path: `${homeUrl}/${getUrlRegExp('terms')}`,
                exact: true,
                disallow: true,
                component: TermsAndConditions,
            },
        ]
    },
];

export default routes;
