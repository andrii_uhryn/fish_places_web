export const selectMode = state => state.root.mode;
export const selectBaseURL = state => state.root.baseURL;
export const selectScrollPosition = state => state.root.scrollPosition;
export const selectCurrentLanguage = state => state.root.currentLanguage;
