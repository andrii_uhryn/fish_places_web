import thunk from 'redux-thunk';
import { routerMiddleware } from 'connected-react-router';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createStore, applyMiddleware } from 'redux';

import createRootReducer from '../reducers';


export const configureStore = ({ history, initialState, middleware = [] }) => {
    const store = createStore(
        createRootReducer(history),
        initialState,
        process.env.NODE_ENV === 'production' ?
            applyMiddleware(...[thunk, routerMiddleware(history)].concat(...middleware))
            :
            composeWithDevTools(
                applyMiddleware(...[thunk, routerMiddleware(history)].concat(...middleware))
            )
    );

    if (process.env.NODE_ENV !== 'production') {
        if (module.hot) {
            module.hot.accept('../reducers', () =>
                store.replaceReducer(require('../reducers').default)
            );
        }
    }

    return store;
};

export default configureStore;
