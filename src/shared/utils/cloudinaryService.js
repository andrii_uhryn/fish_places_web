import { getCookie } from './cookieHelper';
import { fetchRequest, headers, checkIfError, parseResponse, handleError } from './fetchHelper';

export const openUploadWidget = (options, callback, open) => window.cloudinary.openUploadWidget(options, callback, open);

export const removeByToken = tokens => {
    const url = `https://api.cloudinary.com/v1_1/${process.env.CLOUDINARY_NAME}/delete_by_token`;

    tokens.map(item => item.delete_token && fetch(url, {
        body: JSON.stringify({ token: item.delete_token }),
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-Requested-With': 'XMLHttpRequest',
        },
    })
        .then(checkIfError)
        .then(parseResponse)
        .catch(handleError));
};

export const generateSignature = (callback, params_to_sign) => {
    const token = getCookie('token');

    fetchRequest('/mobile/generate_signature', {
        body: JSON.stringify({ params_to_sign }),
        method: 'POST',
        headers: {
            ...headers,
            Authorization: `Token ${token}`,
        },
    })
        .then(resp => callback(resp.token_to_sign))
        .catch(e => {
            callback();
            handleError(e);
        });
};
