import _map from 'lodash/map';
import _compact from 'lodash/compact';
import { toast } from 'react-toastify';

export const checkIfError = res => {
    if (!~[200, 201, 204, 0].indexOf(res.status)) {
        throw res;
    }

    return res;
};

export const parseResponse = res => {
    if (typeof res.json === 'function' && res.headers.get('content-type') === 'application/json') {
        return res.json();
    } else {
        return res.text();
    }
};

export const handleError = e => {
    if (typeof e.json === 'function') {
        e
            .json()
            .then(res => {
                _map(res, (value, key) => {
                    if (Array.isArray(value)) {
                        value.forEach(err => {
                            toast(`${key} - ${err}`, { className: 'error' });
                        });
                    } else if (typeof value === 'string') {
                        toast(`${key} - ${value}`, { className: 'error' });
                    }
                });
            });
    } else if (Object.keys(e).length) {
        _map(e, (value, key) => {
            if (Array.isArray(value)) {
                value.forEach(err => {
                    toast(`${key} - ${err}`, { className: 'error' });
                });
            } else if (typeof value === 'string') {
                toast(`${key} - ${value}`, { className: 'error' });
            }
        });
    } else {
        console.error(e);
    }
};

export const headers = {
    Pragma: 'no-cache',
    Accept: 'application/json',
    Expires: -1,
    'Content-Type': 'application/json',
    'Cache-Control': 'no-cache',
};

export const fetchRequest = (url, options) => fetch(`${process.env.BACKEND_URL}${url}`, options)
    .then(checkIfError)
    .then(parseResponse);

export const getQuery = data => {
    const query = _compact(_map(data, (item, index) => item !== '' && typeof item !== 'undefined' && index && `${index}=${item}`));

    return query.length ? query.join('&') : '';
};
