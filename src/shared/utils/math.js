import { COMMA_REGEXP } from '../constants/regExp';

export const addCommas = (nStr, withDots, noCoins) => {
	if (!nStr) {
		return nStr;
	}
	
	nStr += '';
	const x = nStr.split('.');
	
	let x1 = x[0];
	let x2 = x.length > 1 ? `,${x[1]}` : noCoins ? '' : ',00';
	
	while (COMMA_REGEXP.test(x1)) {
		x1 = x1.replace(COMMA_REGEXP, '$1' + (withDots ? '.' : ',') + '$2');
	}
	
	return x1 + x2;
};
