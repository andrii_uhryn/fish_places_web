import base64ToBlob from './base64ToBlob';
import { checkIfError, parseResponse } from './fetchHelper';

export default ({ token, image, url, method = 'PATCH' }) => {
	const body = base64ToBlob(image);

	return new Promise((resolve, reject) => fetch(url, {
		body,
		method,
		headers: {
			Authorization: `Token ${token}`,
		},
	})
		.then(checkIfError)
		.then(parseResponse)
		.then(resolve)
		.catch(reject));
};