import _forEach from 'lodash/forEach';

export function ClassDotsManager(slickElementId, numOfItemsToShow, numOfDots) {
	this.minIndex = 0;
	this.maxIndex = numOfItemsToShow - 1;
	this.numOfDots = numOfDots;
	this.numOfItemsToShow = numOfItemsToShow;
	this.listDotsElement = document.querySelectorAll(`#${slickElementId} ul.slick-dots li`);

	this.showDotsBetween = function (minIndex, maxIndex) {
		if (this.listDotsElement) {
			_forEach(this.listDotsElement, (item, index) => item.setAttribute('style', `display: ${index >= minIndex && index <= maxIndex ? 'inline-block' : 'none'};`));
		}
	};

	this.init = function () {
		this.showDotsBetween(this.minIndex, this.maxIndex);
	};

	this.updateDots = function (newIndex) {
		const middle = +((this.minIndex + this.maxIndex) / 2).toFixed(0);

		if (middle === newIndex - 1) {
			if (this.maxIndex < this.numOfDots - 1) {
				this.minIndex += 1;
				this.maxIndex += 1;
			}

			this.showDotsBetween(this.minIndex, this.maxIndex);
		} else if (this.minIndex !== 0 && middle === newIndex + 1) {
			this.minIndex -= 1;
			this.maxIndex -= 1;

			this.showDotsBetween(this.minIndex, this.maxIndex);
		} else if (newIndex === this.numOfDots - 1) {
			this.minIndex = this.numOfDots - this.numOfItemsToShow;
			this.maxIndex = this.numOfDots - 1;

			this.showDotsBetween(this.minIndex, this.maxIndex);
		} else if (newIndex === 0) {
			this.minIndex = 0;
			this.maxIndex = this.numOfItemsToShow - 1;

			this.showDotsBetween(this.minIndex, this.maxIndex);
		} else if (newIndex <= this.minIndex || newIndex >= this.maxIndex) {
			const numbMid = +(this.numOfItemsToShow / 2).toFixed(0);
			this.minIndex = newIndex - numbMid + 2;
			this.maxIndex = newIndex + numbMid;

			this.updateDots(newIndex);
		}
	};
}

export default ClassDotsManager;
