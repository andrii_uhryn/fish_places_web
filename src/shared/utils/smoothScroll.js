export const smoothScroll = {
    timer: null,

    stop: function () {
        clearTimeout(this.timer);
    },

    scrollToElementTop: function (id, props, callback) {
        if (!id) {
            return false;
        }
        const node = document.getElementById(id);

        if (!node) {
            return false;
        }

        const settings = {
            duration: 1000,
            ...props
        };
        const targetY = node.scrollTop;
        const startTime = Date.now();
        let percentage = 0;

        if (this.timer) {
            clearInterval(this.timer);
        }

        const step = () => {
            let yScroll;
            const elapsed = Date.now() - startTime;

            if (elapsed > settings.duration) {
                clearTimeout(this.timer);
            }

            percentage = elapsed / settings.duration;

            if (percentage > 1) {
                clearTimeout(this.timer);

                if (callback) {
                    callback();
                }
            } else {
                yScroll = targetY - (targetY * percentage);

                node.scrollTo(0, yScroll);
                this.timer = setTimeout(step, 10);
            }
        };

        this.timer = setTimeout(step, 10);
    },

    scrollToElementById: function (id, props, cb) {
        if (!id) {
            return false;
        }
        const node = document.getElementById(id);

        if (!node) {
            return false;
        }

        const settings = {
            duration: 1000,
            ...props
        };
        const currentScrollY = window.scrollY;
        const startTime = Date.now();

        let percentage = 0;

        if (this.timer) {
            clearInterval(this.timer);
        }

        const step = () => {
            let targetY = +(node.getBoundingClientRect().top + window.pageYOffset).toFixed(0) + (props.mode === 'mobile' ? 30 : 0);
            let yScroll;
            const elapsed = Date.now() - startTime;

            if (elapsed > settings.duration && targetY === currentScrollY) {
                clearTimeout(this.timer);
                typeof cb === 'function' && cb();
            }

            percentage = elapsed / settings.duration;

            if (targetY === currentScrollY || ((window.innerHeight + window.scrollY) >= document.body.offsetHeight && targetY > currentScrollY)) {
                clearTimeout(this.timer);
                typeof cb === 'function' && cb();
            } else {
                if (targetY < window.scrollY) {
                    yScroll = +(currentScrollY - ((5000) * percentage)).toFixed(0);

                    if (yScroll < targetY) {
                        yScroll = targetY;
                    }
                    window.scrollTo(0, yScroll);
                    this.timer = setTimeout(step, 5);
                } else if (targetY > window.scrollY) {
                    yScroll = +(currentScrollY + ((targetY < 3000 ? 3000 : targetY) * percentage)).toFixed(0);

                    if (yScroll > targetY) {
                        yScroll = targetY;
                    }

                    if (yScroll < currentScrollY && targetY > currentScrollY) {
                        this.timer = setTimeout(step, 5);
                    } else {
                        window.scrollTo(0, yScroll);
                        this.timer = setTimeout(step, 5);
                    }
                } else {
                    clearTimeout(this.timer);
                    typeof cb === 'function' && cb();
                }
            }
        };

        this.timer = setTimeout(step, 5);
    },

    scrollTo: function (id, callback) {
        if (!id) {
            return false;
        }

        const node = document.getElementById(id);

        if (!node) {
            return false;
        }

        const settings = {
            duration: 1000,
            easing: {
                outQuint: (x, t, b, c, d) => c * ((t = t / d - 1) * t * t * t * t + 1) + b
            }
        };
        const nodeTop = node.offsetTop;
        const nodeHeight = node.offsetHeight;
        const body = document.body;
        const html = document.documentElement;
        const height = Math.max(
            body.scrollHeight,
            body.offsetHeight,
            html.clientHeight,
            html.scrollHeight,
            html.offsetHeight
        );
        const windowHeight = window.innerHeight;
        const offset = window.pageYOffset;
        const delta = nodeTop - offset;
        const bottomScrollableY = height - windowHeight;
        const targetY = (bottomScrollableY < delta) ? bottomScrollableY - (height - nodeTop - nodeHeight + offset) : delta;
        const startTime = Date.now();
        let percentage = 0;

        if (this.timer) {
            clearInterval(this.timer);
        }

        const step = () => {
            let yScroll;
            const elapsed = Date.now() - startTime;

            if (elapsed > settings.duration) {
                clearTimeout(this.timer);
            }

            percentage = elapsed / settings.duration;

            if (percentage > 1) {
                clearTimeout(this.timer);

                if (callback) {
                    callback();
                }
            } else {
                yScroll = settings.easing.outQuint(0, elapsed, offset, targetY, settings.duration);
                window.scrollTo(0, yScroll);
                this.timer = setTimeout(step, 10);
            }
        };

        this.timer = setTimeout(step, 10);
    }
};

export default smoothScroll;
