import { URL_LENGTH } from '../constants/general';

export const formatStringLength = (str, length = 28, noDots) => {
	return str ? str.length > length ? `${str.slice(0, length - 1)}${noDots ? '' : '...'}` : str : '';
};

export const transformToUrl = (str, lengthKey) => {
	if (typeof str !== 'string') {
		return '';
	}
	const maxLength = URL_LENGTH[lengthKey] || URL_LENGTH.default;
	
	str = str.replace(/^\s+|\s+$/g, ''); // trim
	str = str.toLowerCase();
	
	// remove accents, swap ñ for n, etc
	const from = 'ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;';
	const to = 'aaaaaeeeeeiiiiooooouuuunc------';
	
	for (let i = 0, l = from.length; i < l; i++) {
		str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
	}
	
	str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
		.replace(/\s+/g, '-') // collapse whitespace and replace by -
		.replace(/-+/g, '-'); // collapse dashes
	
	return str.length > maxLength ? formatStringLength(str, maxLength, true) : str;
};

export const copyToClipboard = str => {
	const el = document.createElement('textarea');  // Create a <textarea> element
	el.value = str;                                 // Set its value to the string that you want copied
	el.setAttribute('readonly', '');                // Make it readonly to be tamper-proof
	el.style.position = 'absolute';
	el.style.left = '-9999px';                      // Move outside the screen to make it invisible
	document.body.appendChild(el);                  // Append the <textarea> element to the HTML document
	
	const selected =
		document.getSelection().rangeCount > 0        // Check if there is any content selected previously
			? document.getSelection().getRangeAt(0)     // Store selection if found
			: false;                                    // Mark as false to know no selection existed before
	
	el.select();                                    // Select the <textarea> content
	document.execCommand('copy');                   // Copy - only works as a result of a user action (e.g. click events)
	document.body.removeChild(el);                  // Remove the <textarea> element
	
	if (selected) {                                 // If a selection existed before copying
		document.getSelection().removeAllRanges();    // Unselect everything on the HTML document
		document.getSelection().addRange(selected);   // Restore the original selection
	}
};

export const addHTTPS = str =>{
	const startUrlS = str.slice(0, 8);
	const startUrl = str.slice(0, 7);
	if (startUrlS !== 'https://' && startUrl !== 'http://') {
		str = `https://${str}`;
	}
	return str;
};