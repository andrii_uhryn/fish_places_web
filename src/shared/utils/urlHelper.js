import _map from 'lodash/map';
import _forEach from 'lodash/forEach';
import _findIndex from 'lodash/findIndex';

import IMAGE_SIZES from '../constants/imageSizes';
import translations from '../i18n/locales';
import { transformToUrl } from './string';
import { LANGUAGES, DEFAULT_LANGUAGE } from '../constants/general';

export const getLink = props => {
	let {
		id,
		cms,
		ids = [],
		title,
		titles = [],
		location = { search: '' },
		language,
		newSearch,
		component,
		withQuery,
	} = props;
	let pathname = location.pathname;
	let search = newSearch || location.search;
	let prevLanguage;
	let idInside;
	let titleInside;

	// set default language if wrong is passed

	if (language && !~_findIndex(LANGUAGES, { id: language })) {
		language = DEFAULT_LANGUAGE;
	}

	if (location.pathname && language) {
		let pathArr = location.pathname.split('/');
		prevLanguage = pathArr[cms ? 2 : 1];

		// change language in the url
		pathArr[cms ? 2 : 1] = language;

		// change component url if passed some component or it's not a home url
		if (component) {
			pathArr = pathArr.slice(0, 2);
			// if there is a translation for the component
			const componentKeys = component.split('/');

			if (componentKeys.length > 1) {
				componentKeys.forEach((item, index) => {
					if (translations[language][`url.${item}`]) {
						if (cms && index === 0) {
							pathArr[1] = translations[language][`url.${item}`];
						} else {
							pathArr[2 + index] = translations[language][`url.${item}`];
						}
					} else if (item === 'id') {
						pathArr[2 + index] = id || ids[0];
						idInside = true;
					} else if (item === 'title') {
						pathArr[2 + index] = transformToUrl(title || titles[0], 'title');
						titleInside = true;
					}
				});
			} else if (translations[language][`url.${component}`]) {

				// change the component in the url
				if (component === 'cms') {
					pathArr[1] = translations[language][`url.${component}`];
				} else {
					pathArr[cms ? 3 : 2] = translations[language][`url.${component}`];
				}
			}

			if (cms) {
				pathArr[2] = language;
			}
		} else {
			pathArr.forEach((item, i) => {
				if (i >= 1 && !~_findIndex(LANGUAGES, { id: pathArr[i] })) {
					// find the component name
					_forEach(translations[prevLanguage], (item, index) => {
						item === pathArr[i] && (pathArr[i] = translations[language][index]);
					});
				}
			});
		}

		pathname = pathArr.join('/');
	}

	if ((id && !idInside) || ids.length >= 2) {
		pathname = `${pathname}/${id || ids[1]}`;

		if (ids.length === 3) {
			pathname = `${pathname}/${id || ids[2]}`;
		}
	}

	if (title && !titleInside || titles.length >= 2) {
		pathname = `${pathname}/${transformToUrl(title || titles[1], 'title')}`;

		if (titles.length === 3) {
			pathname = `${pathname}/${transformToUrl(title || titles[2], 'title')}`;
		}
	}

	if (withQuery) {
		return { pathname, search };
	}

	return { pathname };
};

export const getUrlRegExp = key => `(${_map(translations, (item => item[`url.${key}`])).join('|')})`;

export const getWordTranslations = word => {
	const words = [];

	if (word) {
		_forEach(translations, translation => {
			if (translation[word]) {
				words.push(translation[word]);
			}
		});
	}

	return words;
};

export const formatImageUrl = (url, type, imageDpr) => {
    if (!url || !/cloudinary/.test(url)) return url;

    const info = IMAGE_SIZES[type] || IMAGE_SIZES.default;
    let dpr = 1;
    if (imageDpr) {
        dpr = imageDpr;
    } else if (typeof window !== 'undefined') {
        if (window.devicePixelRatio) {
            if ((window.devicePixelRatio > 1) && (window.devicePixelRatio < 2)) {
                dpr = 2;
            } else {
                dpr = window.devicePixelRatio.toFixed(0);
            }
        }
    }

    return url.split('upload/').join(`upload/${info.join('/')}/dpr_${dpr}/f_auto/`);
};
